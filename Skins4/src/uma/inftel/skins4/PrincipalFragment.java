package uma.inftel.skins4;

import uma.inftel.skins4.adapters.TabsPagerAdapter;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import uma.inftel.skins4.sqlite.AplicacionesSQLiteHelper;
import uma.inftel.skins4.sqlite.AplicacionesXGrupoSQLiteHelper;
import uma.inftel.skins4.sqlite.ComponentesXAppSQLiteHelper;
import uma.inftel.skins4.sqlite.GruposSQLiteHelper;
import uma.inftel.skins4.sqlite.UsuariosSQLiteHelper;
import uma.inftel.skins4.sqlite.UsuariosXGrupoSQLiteHelper;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class PrincipalFragment extends FragmentActivity implements TabListener {
	
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	Context c;
	
	// Tab titles
	private String[] tabs = { "Aplicaciones", "Grupos", "Perfil" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inicio);
		
		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}
		
		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inicio, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			UsuarioSingleton.removeInstance();
			SharedPreferences sh = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
			sh.edit().clear().commit();
			eliminarBasesDeDatos();
			
			Intent logout = new Intent(getApplicationContext(), MainActivity.class);
			logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(logout);
			
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		viewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}
	
	public void eliminarBasesDeDatos(){
		//Abrimos la base de datos 'Aplicacion' en modo escritura
        AplicacionesSQLiteHelper adbh = new AplicacionesSQLiteHelper(this, "aplicacion", null, 1);
        UsuariosSQLiteHelper udbh = new UsuariosSQLiteHelper(this, "usuario", null, 1);
        GruposSQLiteHelper gdbh = new GruposSQLiteHelper(this, "grupo", null, 1);
        UsuariosXGrupoSQLiteHelper uXgdbh = new UsuariosXGrupoSQLiteHelper(this, "usuariosxgrupo", null, 1);
        ComponentesXAppSQLiteHelper cXadbh = new ComponentesXAppSQLiteHelper(this, "componentexaplicacion", null, 1);
        AplicacionesXGrupoSQLiteHelper aXgdbh = new AplicacionesXGrupoSQLiteHelper(this, "aplicacionesxgrupo", null, 1);
        
        SQLiteDatabase adb = adbh.getWritableDatabase();
        SQLiteDatabase udb = udbh.getWritableDatabase();
        SQLiteDatabase gdb = gdbh.getWritableDatabase();
        SQLiteDatabase uXgdb = uXgdbh.getWritableDatabase();
        SQLiteDatabase cXadb = cXadbh.getWritableDatabase();
        SQLiteDatabase aXgdb = aXgdbh.getWritableDatabase();
        
        if(adb != null && udb != null && gdb != null && uXgdb != null && cXadb != null && aXgdb != null)
        {
        	AplicacionesSQLiteHelper.eliminarApp(adb);
        	UsuariosSQLiteHelper.eliminarUsuarios(udb);
        	GruposSQLiteHelper.eliminarGrupo(gdb);
        	UsuariosXGrupoSQLiteHelper.eliminarUXG(uXgdb);
        	ComponentesXAppSQLiteHelper.eliminarCXA(cXadb);
        	AplicacionesXGrupoSQLiteHelper.eliminarAXG(aXgdb);
        	
        	//Cerramos las base de datos
            adb.close();
            udb.close();
            gdb.close();
            uXgdb.close();
            cXadb.close();
            aXgdb.close();
        }
		
	}

	public void guardarDatos(){
		UsuarioSingleton usuario = UsuarioSingleton.getInstance();
		
		//Abrimos la base de datos 'Aplicacion' en modo escritura
        AplicacionesSQLiteHelper adbh = new AplicacionesSQLiteHelper(this, "aplicacion", null, 1);
        UsuariosSQLiteHelper udbh = new UsuariosSQLiteHelper(this, "usuario", null, 1);
        GruposSQLiteHelper gdbh = new GruposSQLiteHelper(this, "grupo", null, 1);
        UsuariosXGrupoSQLiteHelper uXgdbh = new UsuariosXGrupoSQLiteHelper(this, "usuariosxgrupo", null, 1);
        ComponentesXAppSQLiteHelper cXadbh = new ComponentesXAppSQLiteHelper(this, "componentexaplicacion", null, 1);
        AplicacionesXGrupoSQLiteHelper aXgdbh = new AplicacionesXGrupoSQLiteHelper(this, "aplicacionesxgrupo", null, 1);
        
        SQLiteDatabase adb = adbh.getWritableDatabase();
        SQLiteDatabase udb = udbh.getWritableDatabase();
        SQLiteDatabase gdb = gdbh.getWritableDatabase();
        SQLiteDatabase uXgdb = uXgdbh.getWritableDatabase();
        SQLiteDatabase cXadb = cXadbh.getWritableDatabase();
        SQLiteDatabase aXgdb = aXgdbh.getWritableDatabase();
        
        //Si hemos abierto correctamente la base de datos
        if(adb != null && udb != null && gdb != null && uXgdb != null && cXadb != null && aXgdb != null)
        {
        	AplicacionesSQLiteHelper.eliminarApp(adb);
        	UsuariosSQLiteHelper.eliminarUsuarios(udb);
        	GruposSQLiteHelper.eliminarGrupo(gdb);
        	UsuariosXGrupoSQLiteHelper.eliminarUXG(uXgdb);
        	ComponentesXAppSQLiteHelper.eliminarCXA(cXadb);
        	AplicacionesXGrupoSQLiteHelper.eliminarAXG(aXgdb);
        	
        	for(Grupo g: usuario.getListaGrupos()){
            	GruposSQLiteHelper.insertarGrupo(adb, udb, gdb, uXgdb, aXgdb, cXadb, g);
        	}
        	
        	for(Aplicacion app: usuario.getListaApp()){
        		AplicacionesSQLiteHelper.insertarApp(adb, cXadb, app);
            	UsuariosSQLiteHelper.insertarUsuario(udb, usuario.getUsuario());
            	GruposSQLiteHelper.insertarGrupo(adb, udb, gdb, uXgdb, aXgdb, cXadb, app.getGrupo());
        	}
        	
            //Cerramos las base de datos
            adb.close();
            udb.close();
            gdb.close();
            uXgdb.close();
            cXadb.close();
            aXgdb.close();
        }
	}
	
	
	public void onDestroy (){
		guardarDatos();
		UsuarioSingleton.removeInstance();
		super.onDestroy();
	}
}
