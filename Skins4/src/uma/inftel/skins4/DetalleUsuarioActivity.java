package uma.inftel.skins4;

import uma.inftel.skins4.modelo.Usuario;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class DetalleUsuarioActivity extends Activity {

	Usuario usuario;
	TextView nombreTV, appellidoTV, loginTV, telefonoTV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle_usuario);
		inicializarVista();
		getActionBar().setDisplayHomeAsUpEnabled(false);
		// Conseguir el mensaje de intent
		//Intent intent = getIntent();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detalleusuario, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
        case R.id.action_settings:
            Log.i("ActionBar", "Settings!");;
            return true;
        default:
            return super.onOptionsItemSelected(item);
	}
	}
	private void inicializarVista() {
		// TODO Auto-generated method stub
		usuario = (Usuario) getIntent().getSerializableExtra("usuario");
		
		nombreTV = (TextView) findViewById(R.id.nombreDU);
		nombreTV.setText(usuario.getNombre());
		appellidoTV = (TextView) findViewById(R.id.apellidosDU);
		appellidoTV.setText(usuario.getApellido());
		loginTV = (TextView) findViewById(R.id.loginDU); 
		loginTV.setText(usuario.getLogin());
		telefonoTV= (TextView) findViewById(R.id.telefonoDU);
		telefonoTV.setText(usuario.getTelefono());
	}

}
