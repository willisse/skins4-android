package uma.inftel.skins4.adapters;

import java.util.List;

import uma.inftel.skins4.R;

import uma.inftel.skins4.modelo.Usuario;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ItemUsuarioAdapter extends ArrayAdapter<Usuario> {
	  private final Context context;
	  private final List<Usuario> values;

	  public ItemUsuarioAdapter(Context context, List<Usuario> values) {
	    super(context, R.layout.usuario_listitem, values);
	    this.context = context;
	    this.values = values;
	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.usuario_listitem, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.nombreUsuario);
	    TextView textViewid = (TextView) rowView.findViewById(R.id.emailUsuario);
	    textView.setText(values.get(position).getNombre());
	    textViewid.setText(values.get(position).getLogin());

	    return rowView;
	  }

}