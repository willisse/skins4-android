package uma.inftel.skins4.adapters;

import uma.inftel.skins4.AplicacionesGrupoFragment;
import uma.inftel.skins4.UsuariosGrupoFragment;
import uma.inftel.skins4.modelo.Grupo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class GrupoTabsPagerAdapter extends FragmentPagerAdapter {
	
	private Grupo g;
	public GrupoTabsPagerAdapter(FragmentManager fm, Grupo grupo) {
		super(fm);
		// TODO Auto-generated constructor stub
		this.g = grupo;
		
	}

	@Override
	public Fragment getItem(int index) {

		 switch (index) {
	        case 0:
	          
	            return new UsuariosGrupoFragment(g);
	        case 1:
	           
	            return new AplicacionesGrupoFragment(g);
	       
	        }
	 
		
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}

}