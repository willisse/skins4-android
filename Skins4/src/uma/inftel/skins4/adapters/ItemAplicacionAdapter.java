package uma.inftel.skins4.adapters;

import java.util.List;

import uma.inftel.skins4.R;
import uma.inftel.skins4.modelo.Aplicacion;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ItemAplicacionAdapter extends ArrayAdapter<Aplicacion> {
	  private final Context context;
	  private final List<Aplicacion> values;

	  public ItemAplicacionAdapter(Context context, List<Aplicacion> values) {
	    super(context, R.layout.aplicacion_listitem, values);
	    this.context = context;
	    this.values = values;
	  }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View rowView = inflater.inflate(R.layout.aplicacion_listitem, parent,false);
		TextView textView = (TextView) rowView.findViewById(R.id.nombreAplicacion);
		TextView textViewid = (TextView) rowView.findViewById(R.id.nombrePropietario);
		textView.setText(values.get(position).getNombre());
		textViewid.setText(values.get(position).getPropietario().getNombre());
	    
	    return rowView;
	  }
	}