package uma.inftel.skins4.adapters;



import java.util.List;

import uma.inftel.skins4.R;
import uma.inftel.skins4.modelo.Grupo;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class ItemGrupoAdapter extends ArrayAdapter<Grupo> {
	  private final Context context;
	  private final List<Grupo> values;

	  public ItemGrupoAdapter(Context context, List<Grupo> values) {
	    super(context, R.layout.grupo_listitem, values);
	    this.context = context;
	    this.values = values;
	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.grupo_listitem, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.nombreGrupo);
	    TextView textViewid = (TextView) rowView.findViewById(R.id.propietarioGrupo);
	    textView.setText(values.get(position).getNombre());
	    textViewid.setText(values.get(position).getId_grupo().toString());

	    return rowView;
	  }
	} 