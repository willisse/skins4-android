package uma.inftel.skins4.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uma.inftel.skins4.AplicacionesFragment;
import uma.inftel.skins4.GruposFragment;
import uma.inftel.skins4.PerfilFragment;


public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {

		 switch (index) {
	        case 0:
	            // Top Rated fragment activity
	            return new AplicacionesFragment();
	        case 1:
	            // Games fragment activity
	            return new GruposFragment();
	        case 2:
	            // Movies fragment activity
	            return new PerfilFragment();
	        }
	 
		
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

}
