package uma.inftel.skins4.adapters;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import uma.inftel.skins4.DetalleAplicacionActivity;
import uma.inftel.skins4.R;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.ComponenteBasico;
import uma.inftel.skins4.modelo.ComponenteTelefono;
import uma.inftel.skins4.modelo.ComponenteTelefonoYTexto;
import uma.inftel.skins4.modelo.ComponenteXAplicacion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.Dialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class GridViewAdapter extends BaseAdapter {

	private final Context mContext;
	private Aplicacion aplicacion;
	private List<ComponenteXAplicacion> componentes;
	private List<ComponenteXAplicacion> componentesIzq;
	private List<ComponenteXAplicacion> componentesDch;
	private final int TAM_GRID = 8;
	private final int TAM_COL = 4;
	public static final int ID_AGENDA = 501;
	public static final int ID_WHATSAPP = 651;

	public GridViewAdapter (DetalleAplicacionActivity c, Aplicacion aplicacion) {
		this.mContext = c;
		this.aplicacion = aplicacion;
		componentes = aplicacion.getListaCompnentes();
		componentesIzq = new ArrayList<ComponenteXAplicacion>();
		componentesDch = new ArrayList<ComponenteXAplicacion>();
		for (ComponenteXAplicacion cxa : componentes){
			if (cxa.getCol().equals("Izq")){
				componentesIzq.add(cxa);
			}
			else{
				componentesDch.add(cxa);
			}
		}
		if (componentesIzq.size() < TAM_COL){
			for (int i = componentesIzq.size(); i<TAM_COL; i++){
				componentesIzq.add(null);
			}
		}
		if (componentesDch.size() < TAM_COL){
			for (int i = componentesDch.size(); i<TAM_COL; i++){
				componentesDch.add(null);
			}
		}
		componentes = new ArrayList<ComponenteXAplicacion>();
		for (int i = 0; i<TAM_GRID; i++){
			if (i % 2 == 0){
				componentes.add(componentesIzq.get(i/2));
			}
			else{
				componentes.add(componentesDch.get(i/2));
			}
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return componentes.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return componentes.get(pos);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) {  // if it's not recycled, initialize some attributes
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(256,256));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(8, 8, 8, 8);
		} else {
			imageView = (ImageView) convertView;
		} 
		if (componentes.get(position) != null){
			imageView.setImageResource(((ComponenteBasico) componentes.get(position).getComponente()).getRutaImagen());
			imageView.setOnClickListener(new MyOnClickListener(position));
			imageView.setContentDescription(((ComponenteBasico)componentes.get(position).getComponente()).getNombre());
		}
		else {
			imageView.setImageResource(R.drawable.nofondo);
			imageView.setOnClickListener(null);
			imageView.setContentDescription("Hueco vacío");
		}
		return imageView;
	}

	private class MyOnClickListener implements OnClickListener {

		private int position;

		public MyOnClickListener (int position){
			this.position = position;
		}

		@Override
		public void onClick(View v) {

			Log.e("PRUEBA", componentes.get(position).getComponente().getClass().toString());
			if (componentes.get(position).getComponente().getClass() == ComponenteTelefonoYTexto.class){
				if (((ComponenteBasico)componentes.get(position).getComponente()).getId_componente() == ID_WHATSAPP){
					if (componentes.get(position).getDetalles() == null){
						raiseDialogWhassap(position);
					}
					else{
						JSONObject detalles;
						try {
							detalles = new JSONObject(componentes.get(position).getDetalles());
							((ComponenteTelefonoYTexto) componentes.get(position).getComponente()).setTelefono(detalles.getString("tel"));
							((ComponenteTelefonoYTexto) componentes.get(position).getComponente()).execute(mContext);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					
				}
				else{
					if (componentes.get(position).getDetalles() == null){
						raiseDialogTelefonoYTexto(position);
					}
					else{
						JSONObject detalles;
						try {
							detalles = new JSONObject(componentes.get(position).getDetalles());
							((ComponenteTelefonoYTexto) componentes.get(position).getComponente()).setTelefono(detalles.getString("tel"));
							((ComponenteTelefonoYTexto) componentes.get(position).getComponente()).setTexto(detalles.getString("tex"));
						} catch (JSONException e) {
							e.printStackTrace();
						}

						((ComponenteTelefonoYTexto) componentes.get(position).getComponente()).execute(mContext);
					}
				}
			}
			else if (componentes.get(position).getComponente().getClass() == ComponenteTelefono.class){
					if (componentes.get(position).getDetalles() == null){
						raiseDialogTelefono(position);
					}
					else{
						JSONObject detalles;
						try {
							detalles = new JSONObject(componentes.get(position).getDetalles());
							((ComponenteTelefono) componentes.get(position).getComponente()).setTelefono(detalles.getString("tel"));
						} catch (JSONException e) {
							e.printStackTrace();
						}

						((ComponenteTelefono) componentes.get(position).getComponente()).execute(mContext);
					}
			}
			else if (componentes.get(position).getComponente().getClass() == ComponenteBasico.class){
					if (((ComponenteBasico)componentes.get(position).getComponente()).getId_componente() == ID_AGENDA ){
							raiseDialog();
							//Intent i = new Intent(mContext, DetalleAplicacionActivity.class);
							//mContext.startActivity(i);
							//((ComponenteBasico) componentes.get(position).getComponente()).execute(mContext);
					}
					else {
						((ComponenteBasico) componentes.get(position).getComponente()).execute(mContext);
					}
			}
		}
	}

	//mandar un sms
	private void raiseDialogTelefonoYTexto(final int position)  {
		final Dialog dialog = new Dialog(mContext);
		dialog.setContentView(R.layout.dialogo_add_contact);
		dialog.setTitle("Configurar Componente");
		final EditText texto = (EditText) dialog.findViewById(R.id.texto);
		final EditText telefono = (EditText) dialog.findViewById(R.id.telefono);
		Button b = (Button) dialog.findViewById(R.id.dialogButtonOK);
		b.setOnClickListener(new OnClickListener() {
			//Possitive click
			@Override
			public void onClick(View v) {
				String textoString = texto.getText().toString();
				String numero = telefono.getText().toString();
				if (textoString != null && !textoString.isEmpty() && numero != null && !numero.isEmpty()){
					JSONObject detalles = new JSONObject();
					try{
						detalles.put("tel", telefono.getText().toString());
						detalles.put("tex", texto.getText().toString());
					}
					catch (JSONException e){
						e.printStackTrace();
					}
					componentes.get(position).setDetalles(detalles.toString());
					dialog.dismiss();
					((ComponenteTelefonoYTexto)componentes.get(position).getComponente()).setTelefono(telefono.getText().toString());
					((ComponenteTelefonoYTexto)componentes.get(position).getComponente()).setTexto(texto.getText().toString());

					((ComponenteTelefonoYTexto)componentes.get(position).getComponente()).execute(mContext);
				}
				else{
					Toast.makeText(mContext, "Debes rellenar todos los campos", Toast.LENGTH_LONG).show();
				}
			}
		});
		Button bCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		bCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	//realizar una llamada
	private void raiseDialogTelefono (final int position)  {
		final Dialog dialog = new Dialog(mContext);
		dialog.setContentView(R.layout.dialogo_call_phone);
		dialog.setTitle("Configurar Componente");
		final EditText telefono = (EditText) dialog.findViewById(R.id.telefono);
		Button b = (Button) dialog.findViewById(R.id.dialogButtonOK);
		b.setOnClickListener(new OnClickListener() {
			//Possitive click
			@Override
			public void onClick(View v) {
				String numero = telefono.getText().toString();
				if (numero != null && !numero.isEmpty()){
					JSONObject detalles = new JSONObject();
					try{
						detalles.put("tel", telefono.getText().toString());
					}
					catch (JSONException e){
						e.printStackTrace();
					}
					componentes.get(position).setDetalles(detalles.toString());
					dialog.dismiss();
					((ComponenteTelefono)componentes.get(position).getComponente()).setTelefono(telefono.getText().toString());

					((ComponenteTelefono)componentes.get(position).getComponente()).execute(mContext);
				}
				else{
					Toast.makeText(mContext, "Debes rellenar el campo para llamar", Toast.LENGTH_LONG).show();
				}
			}
		});
		Button bCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		bCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	//anadir un nuevo contacto a la agenda
	private void raiseDialog() {
		final Dialog dialog = new Dialog(mContext);
		dialog.setContentView(R.layout.dialogo_contact);
		dialog.setTitle("Nuevo contacto");
		final EditText nombre = (EditText) dialog.findViewById(R.id.contacto);
		final EditText telefono = (EditText) dialog.findViewById(R.id.telefono);
		Button b = (Button) dialog.findViewById(R.id.dialogButtonOK);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			
				String textoString = nombre.getText().toString();
				String numero = telefono.getText().toString();
				if (textoString != null && !textoString.isEmpty() && numero != null && !numero.isEmpty()){
					anadirAgenda(nombre.getText().toString(), telefono.getText()
							.toString());
					Intent i = new Intent(mContext, DetalleAplicacionActivity.class);
					mContext.startActivity(i);
					Toast.makeText(mContext, "El contacto se ha a�adido con exito", Toast.LENGTH_LONG).show();
				}
				else{
					Toast.makeText(mContext, "Debes rellenar todos los campos", Toast.LENGTH_LONG).show();
				}
			
			}
		});
		Button bCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		bCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void anadirAgenda(String nombre, String telefono) {
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

		Builder builder = ContentProviderOperation
				.newInsert(RawContacts.CONTENT_URI);
		builder.withValue(RawContacts.ACCOUNT_TYPE, null);
		builder.withValue(RawContacts.ACCOUNT_NAME, null);
		ops.add(builder.build());

		// Name
		builder = ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI);
		builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
		builder.withValue(
				ContactsContract.Data.MIMETYPE,
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
		builder.withValue(
				ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
				nombre);
		ops.add(builder.build());

		// Number
		builder = ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI);
		builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
		builder.withValue(ContactsContract.Data.MIMETYPE,
				ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
		builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
				telefono);
		builder.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
				ContactsContract.CommonDataKinds.Phone.TYPE_HOME);
		ops.add(builder.build());

		ContentProviderResult[] res;
		try {
			res = mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY,
					ops);
			if (res != null && res[0] != null) {
				String uri = res[0].uri.getPath().substring(14);
				Log.e("ID: ", uri);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//mandar un whassap
	private void raiseDialogWhassap(final int position)  {
		final Dialog dialog = new Dialog(mContext);
		dialog.setContentView(R.layout.dialogo_whatsapp);
		dialog.setTitle("Configurar Componente");
		final EditText telefono = (EditText) dialog.findViewById(R.id.telefono);
		Button b = (Button) dialog.findViewById(R.id.dialogButtonOK);
		b.setOnClickListener(new OnClickListener() {
			//Possitive click
			@Override
			public void onClick(View v) {
				String numero = telefono.getText().toString();
				if (numero != null && !numero.isEmpty()){
					JSONObject detalles = new JSONObject();
					try{
						detalles.put("tel", telefono.getText().toString());
					}
					catch (JSONException e){
						e.printStackTrace();
					}
					componentes.get(position).setDetalles(detalles.toString());
					dialog.dismiss();
					((ComponenteTelefonoYTexto)componentes.get(position).getComponente()).setTelefono(telefono.getText().toString());

					//((ComponenteTelefonoYTexto)componentes.get(position).getComponente()).execute(mContext);
	/*Funciona sacando todos los contactos
						Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
					    whatsappIntent.setType("text/plain");
					    whatsappIntent.setPackage("com.whatsapp");
					    whatsappIntent.putExtra(Intent.EXTRA_TEXT, textoString);
					    try {
					        mContext.startActivity(whatsappIntent);
					    } catch (android.content.ActivityNotFoundException ex) {
					        Toast.makeText(mContext, "Whatsapp have not been installed.", Toast.LENGTH_SHORT);
					    }*/

					Uri uri = Uri.parse("smsto:" + numero);
					Intent i = new Intent(Intent.ACTION_SENDTO, uri);
					//i.putExtra("sms_body", textoString);
					i.setPackage("com.whatsapp");  
					mContext.startActivity(i);
				}
				else{
					Toast.makeText(mContext, "Debes rellenar todos los campos", Toast.LENGTH_LONG).show();
				}
			}
		});
		Button bCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
		bCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
