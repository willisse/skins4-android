package uma.inftel.skins4.singleton;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.modelo.Usuario;

public class UsuarioSingleton {

	private static UsuarioSingleton INSTANCE = null;

	private Usuario usuario;
	private List<Aplicacion> listaApp;
	private List<Grupo> listaGrupos;

	private UsuarioSingleton() {
		super();
		usuario = new Usuario();
		listaApp = new ArrayList<Aplicacion>();
		listaGrupos = new ArrayList<Grupo>();
	}

	// creador sincronizado para protegerse de posibles problemas  multi-hilo
	// otra prueba para evitar instanciacion multiple
	private synchronized static void createInstance() {
		if (INSTANCE == null) {
			INSTANCE = new UsuarioSingleton();
		}
	}

	public static UsuarioSingleton getInstance() {
		createInstance();
		return INSTANCE;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Aplicacion> getListaApp() {
		return listaApp;
	}

	public void setListaApp(List<Aplicacion> listaApp) {
		this.listaApp = listaApp;
	}

	public List<Grupo> getListaGrupos() {
		return listaGrupos;
	}

	public void setListaGrupos(List<Grupo> listaGrupos) {
		this.listaGrupos = listaGrupos;
	}
	public static void initUsuario(JSONObject usuario) throws JSONException{

		Usuario u = new Usuario(usuario);
		getInstance().setUsuario(u);
	}

	public static void initApps(JSONArray apps) throws JSONException{

		for(int i = 0; i< apps.length(); i++)
		{

			JSONObject o = apps.getJSONObject(i);
			Aplicacion app = new Aplicacion(o);
			getInstance().getListaApp().add(app);
		}
	}

	public static void removeInstance(){
		INSTANCE = null;
	}

	public static void initGrupos(JSONArray gruposJS) throws JSONException{

		for(int i = 0; i< gruposJS.length(); i++)
		{
			JSONObject o = gruposJS.getJSONObject(i);
			Grupo g = new Grupo(o);

			getInstance().getListaGrupos().add(g);
		}
	}
}