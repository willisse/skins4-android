package uma.inftel.skins4;

//import android.app.Fragment;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v4.app.Fragment;

public class PerfilFragment extends Fragment {
	
	private TextView tv_nombre;
	private TextView tv_apellido;
	private TextView tv_login;
	private TextView tv_telefono;
	private UsuarioSingleton usuario = UsuarioSingleton.getInstance();
	
	@Override
	public View onCreateView(LayoutInflater inflater, 
			ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.perfil, container, false);
		
		tv_nombre = (TextView) view.findViewById(R.id.tv_nombre);
		tv_apellido = (TextView) view.findViewById(R.id.tv_apellido);
		tv_login = (TextView) view.findViewById(R.id.tv_login);
		tv_telefono = (TextView) view.findViewById(R.id.tv_telefono);
		
		tv_nombre.setText(usuario.getUsuario().getNombre());
		tv_apellido.setText(usuario.getUsuario().getApellido());
		tv_login.setText(usuario.getUsuario().getLogin());
		tv_telefono.setText(usuario.getUsuario().getTelefono());
		
		return view;
	}
}
