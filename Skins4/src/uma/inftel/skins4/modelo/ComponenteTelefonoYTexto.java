package uma.inftel.skins4.modelo;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

//Representa a los componentes que necesitan telefono y texto (SMS, Whatsapp...)

public class ComponenteTelefonoYTexto extends ComponenteTelefono {

	public ComponenteTelefonoYTexto(JSONObject o) {
		super(o);
	}

	private static final long serialVersionUID = -1372228419553244863L;
	private String texto;

	public void setTexto (String texto){
		this.texto = texto;
	}

	public String getTexto (){
		return this.texto;
	}

	@Override
	public void execute (Context c) {
		if (this.id_componente == ID_SMS){
			Log.e("EJECUTAR", "Componente seleccionado: mandar SMS");              
		        SmsManager sms = SmsManager.getDefault();
//		        sms.sendTextMessage(String.valueOf(this.telefono), null, this.texto, pi, null); 
		        sms.sendTextMessage(telefono, null, texto, null, null);
		        Toast.makeText(c, "SMS Enviado con exito", Toast.LENGTH_LONG).show();
		}
		else if (this.id_componente == ID_WHATSAPP){
			Log.e("EJECUTAR", "Componente seleccionado: Enviar un whatsapp");
			Uri uri = Uri.parse("smsto:" + telefono);
			Intent i = new Intent(Intent.ACTION_SENDTO, uri);
			//i.putExtra("sms_body", textoString);
			i.setPackage("com.whatsapp");  
			c.startActivity(i);
		}
	}
	
	
}
