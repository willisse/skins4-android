package uma.inftel.skins4.modelo;


import java.io.Serializable;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;
//victorcg88@bitbucket.org/victorcg88/skins4-android.git
import android.util.Log;
import android.widget.Toast;

//Representa a los componentes que unicamente necesitan un numero de telefono (llamar a, por ejemplo)

public class ComponenteTelefono extends ComponenteBasico implements Serializable{

	public ComponenteTelefono(JSONObject o) {
		super(o);
	}

	private static final long serialVersionUID = 6585761263708220997L;
	
	protected String telefono;
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Override
	public void execute (Context c){
		if (this.id_componente == ID_LLAMAR){
			Log.e("EJECUTAR", "Componente seleccionado: llamar a un numero preseleccionado");
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + String.valueOf(this.telefono)));
			c.startActivity(intent);
		}
	}
	
}
