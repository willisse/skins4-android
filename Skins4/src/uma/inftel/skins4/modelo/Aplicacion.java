package uma.inftel.skins4.modelo;

import java.io.Serializable;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class Aplicacion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1861107977833879827L;
	private Integer id_aplicacion;
	private String  nombre;
	private String  descripcion;
	private String  fecha;
	private Usuario propietario;
	private Grupo grupo;
	private List<ComponenteXAplicacion> listaCompnentes;

	
	public Aplicacion(){
		
	}
	
	public Aplicacion(JSONObject o){
		try {
			this.id_aplicacion = o.getInt("idAplicacion");
			this.nombre = o.getString("nombre");
			this.descripcion = o.getString("descripcion");
			this.fecha = o.getString("fecha");
			this.propietario = new Usuario(o.getJSONObject("propietario"));
			if (o.has("grupo"))
				this.grupo = new Grupo(o.getJSONObject("grupo"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Integer getId() {
		return id_aplicacion;
	}
	public void setId(Integer id) {
		this.id_aplicacion = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Usuario getPropietario() {
		return propietario;
	}
	public void setPropietario(Usuario propietario) {
		this.propietario = propietario;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public List<ComponenteXAplicacion> getListaCompnentes() {
		return listaCompnentes;
	}

	public void setListaCompnentes(List<ComponenteXAplicacion> listaCompnentes) {
		this.listaCompnentes = listaCompnentes;
	}
	
	
}
