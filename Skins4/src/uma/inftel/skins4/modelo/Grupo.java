package uma.inftel.skins4.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class Grupo implements Serializable {
    
    /**
	 *
	 */
	private static final long serialVersionUID = -6946455285000274776L;
	private Integer id_grupo;
    private String nombre;
    private List<Aplicacion> aplicacionesGrupo;
    private List<Usuario> usuariosGrupo;
    private Usuario propietario;
    
    public Grupo() {
    }
    
    public Grupo(JSONObject o) {
    	try {
			this.id_grupo = o.getInt("idGrupo");
			this.nombre = o.getString("nombre");
			this.aplicacionesGrupo = new ArrayList<Aplicacion>();
			this.usuariosGrupo = new ArrayList<Usuario>();
			this.propietario = new Usuario(o.getJSONObject("propietario"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
    }
    
    public Grupo(Integer idGrupo, String nombre) {
        this.id_grupo = idGrupo;
        this.nombre = nombre;
    }
    
	public Integer getId_grupo() {
		return id_grupo;
	}
    
	public void setId_grupo(Integer id_grupo) {
		this.id_grupo = id_grupo;
	}
    
	public String getNombre() {
		return nombre;
	}
    
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
	public List<Aplicacion> getAplicacionesGrupo() {
		return aplicacionesGrupo;
	}
    
	public void setAplicacionesGrupo(List<Aplicacion> aplicacionesGrupo) {
		this.aplicacionesGrupo = aplicacionesGrupo;
	}
    
	public List<Usuario> getUsuariosGrupo() {
		return usuariosGrupo;
	}
    
	public void setUsuariosGrupo(List<Usuario> usuariosGrupo) {
		this.usuariosGrupo = usuariosGrupo;
	}
    
	public Usuario getPropietario() {
		return propietario;
	}
    
	public void setPropietario(Usuario propietario) {
		this.propietario = propietario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((aplicacionesGrupo == null) ? 0 : aplicacionesGrupo
						.hashCode());
		result = prime * result
				+ ((id_grupo == null) ? 0 : id_grupo.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result
				+ ((propietario == null) ? 0 : propietario.hashCode());
		result = prime * result
				+ ((usuariosGrupo == null) ? 0 : usuariosGrupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupo other = (Grupo) obj;
		if (aplicacionesGrupo == null) {
			if (other.aplicacionesGrupo != null)
				return false;
		} else if (!aplicacionesGrupo.equals(other.aplicacionesGrupo))
			return false;
		if (id_grupo == null) {
			if (other.id_grupo != null)
				return false;
		} else if (!id_grupo.equals(other.id_grupo))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (propietario == null) {
			if (other.propietario != null)
				return false;
		} else if (!propietario.equals(other.propietario))
			return false;
		if (usuariosGrupo == null) {
			if (other.usuariosGrupo != null)
				return false;
		} else if (!usuariosGrupo.equals(other.usuariosGrupo))
			return false;
		return true;
	}
	
	
}