package uma.inftel.skins4.modelo;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class ComponenteXAplicacion implements Serializable{

	private static final long serialVersionUID = -606056920405631620L;
	private Integer idCXADB;
	private Integer id_CXA;
	private Aplicacion aplicacion;
	private ComponenteBasico componente;
	private String  col;
	private String  detalles;
	
	public ComponenteXAplicacion() {
		
	}
	
	public ComponenteXAplicacion(JSONObject o) {
		try {
			int idComponente = o.getJSONObject("idcomponente").getInt("idComponente");
			this.id_CXA =  o.getInt("idComponentexaplicacion");
			this.aplicacion = new Aplicacion(o.getJSONObject("idaplicacion"));
			if (idComponente == ComponenteBasico.ID_AGENDA || idComponente == ComponenteBasico.ID_FACEBOOK 
					|| idComponente == ComponenteBasico.ID_TWITTER || idComponente == ComponenteBasico.ID_EMERGENCIAS){
				this.componente = new ComponenteBasico (o.getJSONObject("idcomponente"));
			}
			else if (idComponente == ComponenteBasico.ID_LLAMAR){
				this.componente = new ComponenteTelefono(o.getJSONObject("idcomponente"));
			}
			else if (idComponente == ComponenteBasico.ID_SMS || idComponente == ComponenteBasico.ID_WHATSAPP){
				this.componente = new ComponenteTelefonoYTexto(o.getJSONObject("idcomponente"));
			}
			this.col = o.getString("columna");
			if (o.has("detalles"))
				this.detalles = o.getString("detalles");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	public Integer getId_CXA() {
		return id_CXA;
	}
    
	public void setId_CXA(Integer id_CXA) {
		this.id_CXA = id_CXA;
	}
    
	public Aplicacion getAplicacion() {
		return aplicacion;
	}
    
	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}
    
	public ComponenteBasico getComponente() {
		return componente;
	}
    
	public void setComponente(ComponenteBasico componente) {
		this.componente = componente;
	}
    
	public String getCol() {
		return col;
	}
    
	public void setCol(String col) {
		this.col = col;
	}
    
	public String getDetalles() {
		return detalles;
	}
    
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}

	public Integer getIdCXADB() {
		return idCXADB;
	}

	public void setIdCXADB(Integer idCXADB) {
		this.idCXADB = idCXADB;
	}
}
