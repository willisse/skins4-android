package uma.inftel.skins4.modelo;

import java.io.Serializable;
import java.util.StringTokenizer;

import org.json.JSONException;
import org.json.JSONObject;

import uma.inftel.skins4.R;
import uma.inftel.skins4.R.drawable;
import uma.inftel.skins4.social.FacebookActivity;
import uma.inftel.skins4.social.TwitterActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
//diegoinftel@bitbucket.org/victorcg88/skins4-android.git

//Clase que representa los componentes llamar a emergencias y anyadir a agenda

public class ComponenteBasico implements Componente, Serializable {

	private static final long serialVersionUID = -5998961481372003632L;

	public static final int ID_AGENDA = 501;
	public static final int ID_EMERGENCIAS = 526;
	public static final int ID_FACEBOOK = 551;
	public static final int ID_LLAMAR = 576;
	public static final int ID_SMS = 601;
	public static final int ID_TWITTER = 626;
	public static final int ID_WHATSAPP = 651;

	protected Integer id_componente;
	protected String nombre;
	protected Integer rutaImagen;
	protected Context c;

	StringTokenizer st;

	public ComponenteBasico(JSONObject o) {
		try {
			this.id_componente = o.getInt("idComponente");
			this.nombre = o.getString("nombre");
			this.rutaImagen = getDrawable(o.getString("imagen"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ComponenteBasico(Integer id) {

		this.id_componente = id;

		switch (id) {
		case ID_AGENDA:
			this.nombre = "Agenda";
			this.rutaImagen = R.drawable.agenda;
			break;
		case ID_EMERGENCIAS:
			this.nombre = "Emergencias";
			this.rutaImagen = R.drawable.emergencias;
			break;
		case ID_FACEBOOK:
			this.nombre = "Facebook";
			this.rutaImagen = R.drawable.facebook;
			break;
		case ID_LLAMAR:
			this.nombre = "Llamar";
			this.rutaImagen = R.drawable.llamar;
			break;
		case ID_SMS:
			this.nombre = "SMS";
			this.rutaImagen = R.drawable.sms;
			break;
		case ID_TWITTER:
			this.nombre = "Twitter";
			this.rutaImagen = R.drawable.twitter;
			break;
		case ID_WHATSAPP:
			this.nombre = "Whatsapp";
			this.rutaImagen = R.drawable.whatsapp;
			break;
		}
	}

	private Integer getDrawable(String ruta) {

		if (ruta.equalsIgnoreCase("resources\\images\\256x256\\Agenda.png"))
			return R.drawable.agenda;
		else if (ruta
				.equalsIgnoreCase("resources\\images\\256x256\\Emergencias.png"))
			return R.drawable.emergencias;
		else if (ruta
				.equalsIgnoreCase("resources\\images\\256x256\\Facebook.png"))
			return R.drawable.facebook;
		else if (ruta
				.equalsIgnoreCase("resources\\images\\256x256\\Llamar.png"))
			return R.drawable.llamar;
		else if (ruta.equalsIgnoreCase("resources\\images\\256x256\\SMS.png"))
			return R.drawable.sms;
		else if (ruta
				.equalsIgnoreCase("resources\\images\\256x256\\Twitter.png"))
			return R.drawable.twitter;
		else if (ruta
				.equalsIgnoreCase("resources\\images\\256x256\\Whatsapp.png"))
			return R.drawable.whatsapp;
		return null;
	}

	@Override
	public void execute(Context c) {
		if (this.id_componente == ID_EMERGENCIAS) {
			Log.e("EJECUTAR", "Componente seleccionado: llamada a emergencias");
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:112"));
			c.startActivity(intent);
		} else if (this.id_componente == ID_AGENDA) {
			// Mostrar dialogo con los campos nombre y numero
			Log.e("EJECUTAR",
					"Componente seleccionado: a��adir nuevo contacto a la agenda");
		} else if (this.id_componente == ID_FACEBOOK) {
			Log.e("EJECUTAR", "Componente seleccionado: publicar en Facebook");
			Intent i = new Intent(c, FacebookActivity.class);
			c.startActivity(i);

		} else if (this.id_componente == ID_TWITTER) {
			Log.e("EJECUTAR", "Componente seleccionado: publicar en Twitter");
			Intent intent = new Intent(c, TwitterActivity.class);
			intent.putExtra("texto", "Via @Skins4Android");
			c.startActivity(intent);
		}
	}

	/*
	 * private void raiseDialog() { final Dialog dialog = new Dialog(c);
	 * dialog.setContentView(R.layout.dialogo_add_contact);
	 * dialog.setTitle("Nuevo contacto"); final EditText nombre = (EditText)
	 * dialog.findViewById(R.id.texto); final EditText telefono = (EditText)
	 * dialog.findViewById(R.id.telefono); Button b = (Button)
	 * dialog.findViewById(R.id.dialogButtonOK); b.setOnClickListener(new
	 * OnClickListener() {
	 * 
	 * @Override public void onClick(View v) {
	 * anadirAgenda(nombre.getText().toString(), telefono.getText()
	 * .toString()); } }); Button bCancel = (Button)
	 * dialog.findViewById(R.id.dialogButtonCancel);
	 * bCancel.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { dialog.dismiss(); } });
	 * dialog.show(); }
	 * 
	 * private void anadirAgenda(String nombre, String telefono) {
	 * ArrayList<ContentProviderOperation> ops = new
	 * ArrayList<ContentProviderOperation>();
	 * 
	 * Builder builder = ContentProviderOperation
	 * .newInsert(RawContacts.CONTENT_URI);
	 * builder.withValue(RawContacts.ACCOUNT_TYPE, null);
	 * builder.withValue(RawContacts.ACCOUNT_NAME, null);
	 * ops.add(builder.build());
	 * 
	 * // Name builder = ContentProviderOperation
	 * .newInsert(ContactsContract.Data.CONTENT_URI);
	 * builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
	 * builder.withValue( ContactsContract.Data.MIMETYPE,
	 * ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
	 * builder.withValue(
	 * ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, nombre);
	 * ops.add(builder.build());
	 * 
	 * // Number builder = ContentProviderOperation
	 * .newInsert(ContactsContract.Data.CONTENT_URI);
	 * builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
	 * builder.withValue(ContactsContract.Data.MIMETYPE,
	 * ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
	 * builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
	 * telefono); builder.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
	 * ContactsContract.CommonDataKinds.Phone.TYPE_HOME);
	 * ops.add(builder.build());
	 * 
	 * ContentProviderResult[] res; try { res =
	 * c.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops); if
	 * (res != null && res[0] != null) { String uri =
	 * res[0].uri.getPath().substring(14); Log.e("ID: ", uri); } } catch
	 * (Exception e) { e.printStackTrace(); } }
	 */

	public Integer getId_componente() {
		return id_componente;
	}

	public void setId_componente(Integer id_componente) {
		this.id_componente = id_componente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(Integer rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

}
