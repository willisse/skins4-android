package uma.inftel.skins4;

import java.util.ArrayList;
import java.util.List;

import uma.inftel.skins4.adapters.ItemAplicacionAdapter;
import uma.inftel.skins4.asynctasks.FetchComponentesAplicacionAsyncTask;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;


//import android.app.Fragment;

public class AplicacionesFragment extends Fragment {

	
	private ListView lstOpciones;
	
	UsuarioSingleton usuario = UsuarioSingleton.getInstance();
	private Aplicacion appSeleccionada;
	private List<Aplicacion> aplicaciones;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View lview = inflater.inflate(R.layout.aplicaciones, container, false);

//		FetchAplicacionesAsynctask f = new FetchAplicacionesAsynctask(getActivity());
//		f.execute("victor@gmail.com");
		
		aplicaciones = new ArrayList<Aplicacion>(usuario.getListaApp());
	
		
		ItemAplicacionAdapter adaptador = new ItemAplicacionAdapter(lview.getContext(), aplicaciones);

		// lblEtiqueta = (TextView)findViewById(R.id.LblEtiqueta);
		lstOpciones = (ListView) lview.findViewById(R.id.LstOpciones);

		lstOpciones.setAdapter(adaptador);

		lstOpciones.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				
				appSeleccionada = aplicaciones.get(position);
				if(isOnline()){
					Log.i("CONDICIONES", "HAY CONEXION");

					FetchComponentesAplicacionAsyncTask f = new FetchComponentesAplicacionAsyncTask(getActivity(), appSeleccionada);
					f.execute();
				}
				else {
					
					Log.i("CONDICIONES", "Grupo, NO HAY CONEXION");
					if(!appSeleccionada.getListaCompnentes().isEmpty() && appSeleccionada.getListaCompnentes() != null){
						
					Intent intent = new Intent(getActivity(), DetalleAplicacionActivity.class);
					intent.putExtra("aplicacion", appSeleccionada);
					getActivity().startActivity(intent);
					} else {
					
					/**********************
					//CARGAR DATOS DESDE LA BD
					*************************/
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									getActivity());
							
							// set title
							alertDialogBuilder.setTitle("Error de conexion");
				 
							// set dialog message
							alertDialogBuilder
								.setMessage("Conectese a internet para seguir...")
								.setCancelable(false)
								.setIcon(R.drawable.editdelete)
								.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int which) {
						            }
						        });
				 
								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder.create();
				 
								// show it
								alertDialog.show();
							

				
					}
					}
					}
					
		});
		return lview;
	}

			public boolean isOnline() {
				ConnectivityManager cm = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo netInfo = cm.getActiveNetworkInfo();
				if (netInfo != null && netInfo.isConnectedOrConnecting()) {
					return true;
				}
				return false;
			}


}