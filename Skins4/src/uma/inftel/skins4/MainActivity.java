package uma.inftel.skins4;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import uma.inftel.skins4.asynctasks.CargarTodoAsyncTask;
import uma.inftel.skins4.asynctasks.DoLoginAsyncTask;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import uma.inftel.skins4.sqlite.AplicacionesSQLiteHelper;
import uma.inftel.skins4.sqlite.AplicacionesXGrupoSQLiteHelper;
import uma.inftel.skins4.sqlite.ComponentesXAppSQLiteHelper;
import uma.inftel.skins4.sqlite.GruposSQLiteHelper;
import uma.inftel.skins4.sqlite.UsuariosSQLiteHelper;
import uma.inftel.skins4.sqlite.UsuariosXGrupoSQLiteHelper;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
//diegoinftel@bitbucket.org/victorcg88/skins4-android.git
//diegoinftel@bitbucket.org/victorcg88/skins4-android.git

public class MainActivity extends Activity {
	
	private EditText et_user;
	private EditText et_password;
	private Button btn_entrar;
	private UsuarioSingleton usuario = UsuarioSingleton.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		
		SharedPreferences prefe = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
		String user = prefe.getString("user", null);
		
		if(user == null){
			
			if(isOnline()){
				Log.i("CONDICIONES", "NO HAY USUARIO, HAY CONEXION");
				/**********************
				//HACER LOGIN CARGAR DATOS DESDE SERVIDOR Y GUARDARLOS EN LA BD
				*************************/
				btn_entrar = (Button) findViewById(R.id.bt_entrar);
				btn_entrar.setOnClickListener(new OnClickListener(){
		            public void onClick(View v) {
		            	logIn();
		            }
	
		        });
			}
			else {
				Log.i("CONDICIONES", "NO HAY USUARIO, NO HAY CONEXION");
				/**********************
				//MOSTRAR MENSAJE DE CONECTAR A INTERNET
				*************************/
			}
		}
		else
		{
			//Como hay usuario en SharedPreferences lo inicializamos en nuestro patr��n singleton, as�� lo tendremos ya cargado en memoria,
			//disponible para toda la aplicaci��n
			try {
				UsuarioSingleton.initUsuario(new JSONObject(user));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(isOnline()){
				Log.i("CONDICIONES", "HAY USUARIO, HAY CONEXION");
				/**********************
				//CARGAR DATOS DESDE SERVIDOR Y GUARDARLOS EN LA BD
				*************************/
				
				CargarTodoAsyncTask ct = new CargarTodoAsyncTask(MainActivity.this);
				ct.execute();
			}
			else {
				Log.i("CONDICIONES", "HAY USUARIO, NO HAY CONEXION");
				/**********************
				//CARGAR DATOS DESDE LA BD
				*************************/
				obtenerDatos();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}

		return false;
	}
	
	public void logIn (){
		et_user = (EditText) findViewById(R.id.user);
		et_password = (EditText) findViewById(R.id.pass);
		
		String user = et_user.getText().toString();
		String pass = et_password.getText().toString();
		
		DoLoginAsyncTask l = new DoLoginAsyncTask(MainActivity.this);
		l.execute(user, pass);
		
		Log.i("Guardar en BD","Guardando");
	}
	
	public void obtenerDatos(){
				
		//Abrimos la base de datos 'Aplicacion' en modo lectura
        AplicacionesSQLiteHelper adbh = new AplicacionesSQLiteHelper(this, "aplicacion", null, 1);
        UsuariosSQLiteHelper udbh = new UsuariosSQLiteHelper(this, "usuario", null, 1);
        GruposSQLiteHelper gdbh = new GruposSQLiteHelper(this, "grupo", null, 1);
        UsuariosXGrupoSQLiteHelper uxgdbh = new UsuariosXGrupoSQLiteHelper(this, "usuariosxgrupo", null, 1);
        ComponentesXAppSQLiteHelper cxadbh = new ComponentesXAppSQLiteHelper(this, "componentexaplicacion", null, 1);
        AplicacionesXGrupoSQLiteHelper aXgdbh = new AplicacionesXGrupoSQLiteHelper(this, "aplicacionesxgrupo", null, 1);
        
        SQLiteDatabase adb = adbh.getReadableDatabase();
        SQLiteDatabase udb = udbh.getReadableDatabase();
        SQLiteDatabase gdb = gdbh.getReadableDatabase();
        SQLiteDatabase uXgdb = uxgdbh.getReadableDatabase();
        SQLiteDatabase cXadb = cxadbh.getReadableDatabase();
        SQLiteDatabase aXgdb = aXgdbh.getReadableDatabase();
        
        //Si hemos abierto correctamente la base de datos
        if(adb != null && udb != null && gdb != null && uXgdb != null && cXadb != null && aXgdb != null)
        {
        	List<Aplicacion> apps =  AplicacionesSQLiteHelper.obtenerListaApp( adb,  udb,  gdb,  aXgdb, cXadb, uXgdb);
        	List<Grupo> grupos = GruposSQLiteHelper.obtenerListaGrupos(adb, udb, gdb, aXgdb, uXgdb);
        	
        	
        	usuario.setListaApp(apps);
        	usuario.setListaGrupos(grupos);
        	
            //Cerramos la base de datos
        	adb.close();
        	udb.close();
        	gdb.close();
        	uXgdb.close();
            cXadb.close();
            aXgdb.close();
        }
        
        Intent i = new Intent(this, PrincipalFragment.class);
		startActivity(i);
	}
	
}
