package uma.inftel.skins4;

import uma.inftel.skins4.adapters.ItemUsuarioAdapter;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


@SuppressLint("ValidFragment")
public class UsuariosGrupoFragment extends Fragment {
	
	UsuarioSingleton usuario = UsuarioSingleton.getInstance();

	
	private ListView lstUsuarios;
	private Grupo grupo;
	
	public UsuariosGrupoFragment(Grupo g) {
		this.grupo = g;
		}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			View lview = inflater.inflate(R.layout.usuariosgrupo, container, false);
			
			
			ItemUsuarioAdapter adaptador = new ItemUsuarioAdapter(lview.getContext(), grupo.getUsuariosGrupo());

			lstUsuarios = (ListView) lview.findViewById(R.id.LstUsuariosGrupo);

			lstUsuarios.setAdapter(adaptador);

			lstUsuarios.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> a, View v, int position,
						long id) {

					// iniciar DetalleUsuarioActivity
					Intent intent = new Intent(v.getContext(),
							DetalleUsuarioActivity.class);
							intent.putExtra("usuario", grupo.getUsuariosGrupo().get(position));
							startActivity(intent);

				}
			});
			
			
			return lview;
		}
		
	
		
}