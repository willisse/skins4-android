package uma.inftel.skins4.conexiones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

import android.util.Log;

////Envio de mensajes por whatsapp
//CommunicationIntent con = new CommunicationIntent(c, "Orchestrator", 7000, 10);
//con.setParams("package", "com.whatsapp"); //Paquete al que mandaremos el mensaje. sera com.whatsapp
//con.setParams("txt", "prueba"); //Texto que mandaremos
//con.setParams("contact", "aaa"); //Nombre del contacto en la agenda
//con.sendEvent(c);
//Probar con 905000 tambien, que parece que abre el contact picker


public class Conexion {

	private final String METHOD_GET = "GET";
	private final String METHOD_POST = "POST";
	private final int TIMEOUT = 10000;
	private HttpURLConnection _conexion;

	public String doConnection (String url, HashMap<String, String> params, String method) throws IOException {

		String line;
		StringBuffer jsonString = new StringBuffer();		

		if (method.equals(METHOD_GET)){
			StringBuffer urlModificable = new StringBuffer(url);
			urlModificable.append('?');
			for (Entry<String, String> entry : params.entrySet()){
				urlModificable.append(entry.getKey() + "=" + entry.getValue() + "&");
			}
			//Quitamos el ultimo caracter (&)
			urlModificable.setLength(urlModificable.length()-1);
			URL u = new URL(urlModificable.toString());
			_conexion = (HttpURLConnection) u.openConnection();
			_conexion.setConnectTimeout(TIMEOUT);
			_conexion.setRequestMethod(METHOD_GET);


		}
		else if (method.equals(METHOD_POST)){
			URL u = new URL(url.toString());
			_conexion = (HttpURLConnection) u.openConnection();
			_conexion.setConnectTimeout(TIMEOUT);
			_conexion.setRequestMethod(METHOD_POST);
			_conexion.setRequestProperty("Accept", "application/json");
			_conexion.setRequestProperty("Content-Type", "application/json");
			_conexion.setDoInput(true);
			_conexion.setDoOutput(true);
			for (Entry<String, String> entry : params.entrySet()){
				_conexion.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
		else{
			//Metodos HTTP no soportados
			throw new UnsupportedOperationException();
		}

		int statusCode = _conexion.getResponseCode();
		Log.e("StatusCode", String.valueOf(statusCode));
		if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
			Log.e("Error 403", "Acceso no autorizado");
		} else if (statusCode != HttpURLConnection.HTTP_OK) {
			Log.e("Error: ", String.valueOf(statusCode));
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(_conexion.getInputStream()));
		while ((line = br.readLine()) != null) {
			jsonString.append(line);
		}
		br.close();
		_conexion.disconnect();
		return jsonString.toString();
	}

}
