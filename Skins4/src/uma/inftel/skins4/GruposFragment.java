package uma.inftel.skins4;

import java.util.ArrayList;
import java.util.List;

import uma.inftel.skins4.adapters.ItemGrupoAdapter;
import uma.inftel.skins4.asynctasks.FetchUsuariosFromGrupo;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class GruposFragment extends Fragment {
	UsuarioSingleton usuario = UsuarioSingleton.getInstance();

	private List<Grupo> grupos;
	private Grupo selectedGrupo;
	ListView lstGrupos;
	
	@Override
	public View onCreateView(LayoutInflater inflater, 
			ViewGroup container, Bundle savedInstanceState) {
		View lview = inflater.inflate(R.layout.grupos, container, false);
		grupos = new ArrayList<Grupo>(usuario.getListaGrupos());

		ItemGrupoAdapter adaptador = new ItemGrupoAdapter(lview.getContext(), grupos);

		lstGrupos = (ListView) lview.findViewById(R.id.LstOpciones);

		lstGrupos.setAdapter(adaptador);

		lstGrupos.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				
				selectedGrupo = usuario.getListaGrupos().get(position);
				if(isOnline()){
					Log.i("CONDICIONES", "HAY CONEXION");
					FetchUsuariosFromGrupo tsk = new FetchUsuariosFromGrupo(getActivity(), selectedGrupo);
					tsk.execute();
				}
				else {
					
					Log.i("CONDICIONES", "Grupo, NO HAY CONEXION");
					if(!selectedGrupo.getUsuariosGrupo().isEmpty() && !selectedGrupo.getAplicacionesGrupo().isEmpty()){
						
					Intent intent = new Intent(getActivity(), DetalleGrupoFragment.class);
					intent.putExtra("grupo", selectedGrupo);
					getActivity().startActivity(intent);
					}
						else{
					
					/**********************
					//CARGAR DATOS DESDE LA BD
					*************************/
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									getActivity());
							
							// set title
							alertDialogBuilder.setTitle("Error de conexion");
				 
							// set dialog message
							alertDialogBuilder
								.setMessage("Conectese a internet para seguir...")
								.setCancelable(false)
								.setIcon(R.drawable.editdelete)
								.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int which) {
						            }
						        });
				 
								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder.create();
				 
								// show it
								alertDialog.show();
							
							
						/*	
					 AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
					 
				        // Setting Dialog Title
					
				        alertDialog.setTitle("Error de conexion");
				 
				        // Setting Dialog Message
				        alertDialog.setMessage("No hay conexion a internet, intentelo m��s tarde.");
				         
				        // Setting alert dialog icon
				        alertDialog.setIcon(R.drawable.editdelete);
				       
				        // Setting OK Button
				        alertDialog.setButton("Aceptar", new DialogInterface.OnClickListener() {
				            public void onClick(DialogInterface dialog, int which) {
				            }
				        });
				 
				        // Showing Alert Message
				        alertDialog.show();*/
					}
					
					
					
				}
				
				
				

			}
		});
		
   
		return lview;
	}
	
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}