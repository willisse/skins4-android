package uma.inftel.skins4;

import uma.inftel.skins4.adapters.ItemAplicacionAdapter;
import uma.inftel.skins4.asynctasks.FetchComponentesAplicacionAsyncTask;
import uma.inftel.skins4.asynctasks.FetchUsuariosFromGrupo;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Grupo;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

@SuppressLint("ValidFragment")
public class AplicacionesGrupoFragment extends Fragment {

	//UsuarioSingleton usuario = UsuarioSingleton.getInstance();
	ListView lstAplicaciones;
	private Grupo grupo;
	private Aplicacion appSeleccionada;

	public AplicacionesGrupoFragment(Grupo g) {

		this.grupo = g;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View lview = inflater.inflate(R.layout.aplicacionesgrupo, container,
				false);

		ItemAplicacionAdapter adaptador = new ItemAplicacionAdapter(
				lview.getContext(), grupo.getAplicacionesGrupo());

		lstAplicaciones = (ListView) lview
				.findViewById(R.id.LstAplicacionesGrupo);

		lstAplicaciones.setAdapter(adaptador);

		lstAplicaciones.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> a, View v, int position,
					long id) {
				appSeleccionada = grupo.getAplicacionesGrupo().get(position);
				// iniciar DetalleActividadActivity
				
				
				if(isOnline()){
					Log.i("CONDICIONES", "HAY CONEXION");
					FetchComponentesAplicacionAsyncTask f = new FetchComponentesAplicacionAsyncTask(getActivity(), appSeleccionada);
					f.execute(grupo.getAplicacionesGrupo().get(position).getId());
				}
				else {
					
					Log.i("CONDICIONES", "Grupo, NO HAY CONEXION");
					if(null!=appSeleccionada.getListaCompnentes()){
						
					Intent intent = new Intent(getActivity(), DetalleAplicacionActivity.class);
					intent.putExtra("aplicacion", appSeleccionada);
					getActivity().startActivity(intent);
					} else {
					
					/**********************
					//CARGAR DATOS DESDE LA BD
					*************************/
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									getActivity());
							
							// set title
							alertDialogBuilder.setTitle("Error de conexion");
				 
							// set dialog message
							alertDialogBuilder
								.setMessage("Conectese a internet para seguir...")
								.setCancelable(false)
								.setIcon(R.drawable.editdelete)
								.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int which) {
						            }
						        });
				 
								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder.create();
				 
								// show it
								alertDialog.show();
							
							
						/*	
					 AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
					 
				        // Setting Dialog Title
					
				        alertDialog.setTitle("Error de conexion");
				 
				        // Setting Dialog Message
				        alertDialog.setMessage("No hay conexion a internet, intentelo más tarde.");
				         
				        // Setting alert dialog icon
				        alertDialog.setIcon(R.drawable.editdelete);
				       
				        // Setting OK Button
				        alertDialog.setButton("Aceptar", new DialogInterface.OnClickListener() {
				            public void onClick(DialogInterface dialog, int which) {
				            }
				        });
				 
				        // Showing Alert Message
				        alertDialog.show();*/
					}
					
					
					
				}
				
				
				

			
				
				
				
				
				
			}
		});

		return lview;
	}
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
}