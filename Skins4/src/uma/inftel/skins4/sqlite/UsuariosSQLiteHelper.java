package uma.inftel.skins4.sqlite;

import uma.inftel.skins4.modelo.Usuario;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UsuariosSQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME = "usuario";
	public static final String COLUMN_NAME_ID_USUARIO = "id_usuario";
	public static final String COLUMN_NAME_LOGIN = " login";
	public static final String COLUMN_NAME_NOMBRE = " nombre";
	public static final String COLUMN_NAME_APELLIDO = " apellido";
	public static final String COLUMN_NAME_TELEFONO = " telefono";

	private static final String TEXT_TYPE = " TEXT";
	private static final String COMMA_SEP = ",";

	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" 
			+ COLUMN_NAME_ID_USUARIO + " INTEGER PRIMARY KEY,"
			+ COLUMN_NAME_LOGIN + TEXT_TYPE + COMMA_SEP 
			+ COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP 
			+ COLUMN_NAME_APELLIDO + TEXT_TYPE + COMMA_SEP 
			+ COLUMN_NAME_TELEFONO + TEXT_TYPE + " )";

	private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;

	private static final String SQL_TRUNCATE_TABLE = "DELETE FROM "
			+ TABLE_NAME;

	public UsuariosSQLiteHelper(Context contexto, String nombre,
			CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Se ejecuta la sentencia SQL de creaci?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior,
			int versionNueva) {
		// NOTA: Por simplicidad del ejemplo aqu? utilizamos directamente la
		// opci?n de
		// eliminar la tabla anterior y crearla de nuevo vac?a con el nuevo
		// formato.
		// Sin embargo lo normal ser? que haya que migrar datos de la tabla
		// antigua
		// a la nueva, por lo que este m?todo deber?a ser m?s elaborado.

		// Se elimina la versi?n anterior de la tabla
		db.execSQL(SQL_DELETE_TABLE);

		// Se crea la nueva versi?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}

	public static void insertarUsuario(SQLiteDatabase udb, Usuario user) {

		if (!user.equals(obtenerUsuario(udb, user.getId_usuario()))) {
			ContentValues nuevoRegistro = new ContentValues();
			nuevoRegistro.put("id_usuario", user.getId_usuario());
			nuevoRegistro.put("login", user.getLogin());
			nuevoRegistro.put("nombre", user.getNombre());
			nuevoRegistro.put("apellido", user.getApellido());
			nuevoRegistro.put("telefono", user.getTelefono());

			try {
				udb.insert(TABLE_NAME, null, nuevoRegistro);
			} catch (Exception e) {
				Log.i("APPS BD", "usuario: " + e);
			}
		}
		// db.update(TABLE_NAME, nuevoRegistro, null, null);
	}

	public static Usuario obtenerUsuario(SQLiteDatabase udb, Integer id) {

		
		String[] args = new String[] { id.toString() };

		Cursor c = udb.query(TABLE_NAME, null, "id_usuario=?", args, null,
				null, null);
		
			if (c.moveToFirst()) {
				Usuario usuario = new Usuario();
				usuario.setId_usuario(c.getInt(0));
				usuario.setLogin(c.getString(1));
				usuario.setNombre(c.getString(2));
				usuario.setApellido(c.getString(3));
				usuario.setTelefono(c.getString(4));
				c.close();
				return usuario;
			}else{
				
				return null;
			}
		

		
	}

	public void obtenerListaUsuarios(SQLiteDatabase udb) {

		// Alternativa 1: m?todo rawQuery()
		Cursor contenido = udb.rawQuery("SELECT * FROM " + TABLE_NAME, null);

		try {
			if (contenido.moveToFirst()) {
				// Recorremos el cursor hasta que no haya m?s registros
				do {
					Usuario u = new Usuario();
					u.setId_usuario(contenido.getInt(0));
					u.setLogin(contenido.getString(1));
					u.setNombre(contenido.getString(2));
					u.setApellido(contenido.getString(3));
					u.setTelefono(contenido.getString(4));

				} while (contenido.moveToNext());
			}
		} finally {
			contenido.close();
		}

	}

	public static void eliminarUsuarios(SQLiteDatabase udb) {
		try {
			udb.execSQL(SQL_TRUNCATE_TABLE);
		} catch (SQLException e) {
			Log.e("SQLEXCEPTION", "" + e);
		}
	}
}
