package uma.inftel.skins4.sqlite;

import java.util.ArrayList;
import java.util.List;

import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Usuario;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UsuariosXGrupoSQLiteHelper extends SQLiteOpenHelper{
	
	public static final String TABLE_NAME = "usuariosxgrupo";
	public static final String COLUMN_NAME_ID_USUARIOXGRUPO = "id_usuarioxgrupo";
	public static final String COLUMN_NAME_ID_GRUPO = " id_grupo";
	public static final String COLUMN_NAME_ID_USUARIO = " id_usuario";

	private static final String COMMA_SEP = ",";
    private static final String INTEGER_TYPE = " INTEGER";

	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" 
			+ COLUMN_NAME_ID_USUARIOXGRUPO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ COLUMN_NAME_ID_GRUPO + INTEGER_TYPE + COMMA_SEP 
			+ COLUMN_NAME_ID_USUARIO + INTEGER_TYPE
			+ " )";

	private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;
	
    private static final String SQL_TRUNCATE_TABLE =
    	    "DELETE FROM " + TABLE_NAME;
	
	public UsuariosXGrupoSQLiteHelper(Context contexto, String nombre, CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Se ejecuta la sentencia SQL de creaci?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior,
			int versionNueva) {
		// NOTA: Por simplicidad del ejemplo aqu? utilizamos directamente la
		// opci?n de
		// eliminar la tabla anterior y crearla de nuevo vac?a con el nuevo
		// formato.
		// Sin embargo lo normal ser? que haya que migrar datos de la tabla
		// antigua
		// a la nueva, por lo que este m?todo deber?a ser m?s elaborado.

		// Se elimina la versi?n anterior de la tabla
		db.execSQL(SQL_DELETE_TABLE);

		// Se crea la nueva versi?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}
		
	

	public static void insertarMiembro(SQLiteDatabase uXgdb, Integer id_grupo, Integer id_usuario) {
		ContentValues nuevoRegistro = new ContentValues();
		
		nuevoRegistro.put("id_grupo", id_grupo);
		nuevoRegistro.put("id_usuario", id_usuario);

		try {
			uXgdb.insert(TABLE_NAME, null, nuevoRegistro);
		} catch (Exception e) {
			Log.i("APPS BD", "UXG: " + e);
		}
	}
	
	public static List<Usuario> obtenerListaUsuarios(SQLiteDatabase udb, SQLiteDatabase uxgdb, Integer idGrupo) {

		List<Usuario> usuarios = new ArrayList<Usuario>();
		String[] args = new String[] { idGrupo.toString() };

		Cursor c = uxgdb.query(TABLE_NAME, null, "id_grupo=?", args, null, null,null);

		try {
			if (c.moveToFirst()) {
				// Recorremos el cursor hasta que no haya m?s registros
				do { 
					Usuario u = UsuariosSQLiteHelper.obtenerUsuario(udb, c.getInt(2));
					usuarios.add(u);
				} while (c.moveToNext());
			}
		} finally {
			c.close();
		}


		return usuarios;
	}
	
	public static void obtenerUXG(SQLiteDatabase uXgdb) {

		// Alternativa 1: m?todo rawQuery()
		Cursor contenido = uXgdb.rawQuery("SELECT * FROM " + TABLE_NAME, null);

		try {
			if (contenido.moveToFirst()) {
				// Recorremos el cursor hasta que no haya m?s registros
				do {
					
					Log.i("UXG","id_usuarioxgrupo: "+contenido.getInt(0)+" id_grupo: "+contenido.getInt(1)+" id_usuario: "+contenido.getInt(2));
					
				} while (contenido.moveToNext());
			}
		} finally {
			contenido.close();
		}

	}
	
	public static void eliminarUXG(SQLiteDatabase uXgdb) {
		try {
			uXgdb.execSQL(SQL_TRUNCATE_TABLE);
		} catch (SQLException e) {
			Log.e("SQLEXCEPTION", "" + e);
		}
	}
}
