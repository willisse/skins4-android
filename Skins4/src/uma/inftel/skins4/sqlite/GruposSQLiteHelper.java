package uma.inftel.skins4.sqlite;

import java.util.ArrayList;
import java.util.List;

import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.modelo.Usuario;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class GruposSQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME = "grupo";
	public static final String COLUMN_NAME_ID_GRUPO = "id_grupo";
	public static final String COLUMN_NAME_NOMBRE = " nombre";
	public static final String COLUMN_NAME_APPGRUPO = " aplicacionesGrupo";
	public static final String COLUMN_NAME_USUARIOSGRUPO = " usuariosGrupo";
	public static final String COLUMN_NAME_PROPIETARIO = " propietario";

	private static final String TEXT_TYPE = " TEXT";
	private static final String COMMA_SEP = ",";

	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
			+ " (" + COLUMN_NAME_ID_GRUPO + " INTEGER PRIMARY KEY,"
			+ COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP + COLUMN_NAME_APPGRUPO
			+ TEXT_TYPE + COMMA_SEP + COLUMN_NAME_USUARIOSGRUPO + TEXT_TYPE
			+ COMMA_SEP + COLUMN_NAME_PROPIETARIO + TEXT_TYPE + " )";

	private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;

	private static final String SQL_TRUNCATE_TABLE = "DELETE FROM "
			+ TABLE_NAME;

	public GruposSQLiteHelper(Context contexto, String nombre,
			CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Se ejecuta la sentencia SQL de creaci?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior,
			int versionNueva) {
		
		db.execSQL(SQL_DELETE_TABLE);

		
		db.execSQL(SQL_CREATE_TABLE);
	}

	public static void insertarGrupo(SQLiteDatabase adb, SQLiteDatabase udb,
			SQLiteDatabase gdb, SQLiteDatabase uXgdb, SQLiteDatabase aXgdb, SQLiteDatabase cXadb,
			Grupo grupo) {

		if (grupo!=null && null==obtenerGrupo(adb, udb, gdb, aXgdb, uXgdb, grupo.getId_grupo())) {
			
			ContentValues nuevoRegistro = new ContentValues();
			nuevoRegistro.put("id_grupo", grupo.getId_grupo());
			nuevoRegistro.put("nombre", grupo.getNombre());
			
			if(grupo.getAplicacionesGrupo() != null && !grupo.getAplicacionesGrupo().isEmpty()){
				for (Aplicacion a : grupo.getAplicacionesGrupo()) {
					
					if (null==AplicacionesSQLiteHelper.obtenerApp(adb, udb, gdb, aXgdb, uXgdb, a.getId()))
						AplicacionesSQLiteHelper.insertarApp(adb, cXadb, a);
						AplicacionesXGrupoSQLiteHelper.insertarAppMiembro(aXgdb, grupo.getId_grupo(), a.getId());
				}
			}
			
			if(grupo.getUsuariosGrupo() != null && !grupo.getUsuariosGrupo().isEmpty()){
				for (Usuario u : grupo.getUsuariosGrupo()) {
					
					if (!u.equals(UsuariosSQLiteHelper.obtenerUsuario(udb, u.getId_usuario())))
						UsuariosSQLiteHelper.insertarUsuario(udb, u);
					
					UsuariosXGrupoSQLiteHelper.insertarMiembro(uXgdb, grupo.getId_grupo(), u.getId_usuario());
				}
			}
			
			if(grupo.getPropietario() != null)
				nuevoRegistro.put("propietario", grupo.getPropietario().getId_usuario());
			////Hay que guardar el usuario propietario en caso de que no exista

			try {
				gdb.insert(TABLE_NAME, null, nuevoRegistro);
			} catch (SQLiteConstraintException e) {
				Log.i("APPS BD", "Grupo: " + e);
			}
		}
	}

	public static Grupo obtenerGrupo(SQLiteDatabase adb, SQLiteDatabase udb, SQLiteDatabase gdb, SQLiteDatabase axgdb, SQLiteDatabase uxgdb, Integer id) {

		Grupo g = null;
		String[] args = new String[] { id.toString() };

		Cursor c = gdb.query(TABLE_NAME, null, "id_grupo=?", args, null, null,null);

		try {
			if (c.moveToFirst()) {
				g = new Grupo();
				g.setId_grupo(c.getInt(0));
				g.setNombre(c.getString(1));
				g.setAplicacionesGrupo(AplicacionesXGrupoSQLiteHelper.obtenerListaApp(adb, udb, gdb, axgdb, uxgdb, c.getInt(0)));
				g.setUsuariosGrupo(UsuariosXGrupoSQLiteHelper.obtenerListaUsuarios(udb, uxgdb, c.getInt(0)));
				g.setPropietario(UsuariosSQLiteHelper.obtenerUsuario(udb, c.getInt(4)));
			}
		} finally {
			c.close();
		}

		return g;
	}
	
	public static Grupo obtenerGrupoSinApp(SQLiteDatabase adb, SQLiteDatabase udb, SQLiteDatabase gdb, SQLiteDatabase axgdb, SQLiteDatabase uxgdb, Integer id) {

		Grupo g = new Grupo();
		String[] args = new String[] { id.toString() };

		Cursor c = gdb.query(TABLE_NAME, null, "id_grupo=?", args, null, null,null);

		try {
			if (c.moveToFirst()) {
				g.setId_grupo(c.getInt(0));
				g.setNombre(c.getString(1));
				g.setPropietario(UsuariosSQLiteHelper.obtenerUsuario(udb, c.getInt(4)));
			}
		} finally {
			c.close();
		}

		return g;
	}

	public static List<Grupo> obtenerListaGrupos(SQLiteDatabase adb, SQLiteDatabase udb, SQLiteDatabase gdb, SQLiteDatabase axgdb, SQLiteDatabase uxgdb) {

		List<Grupo> grupos = new ArrayList<Grupo>();

		// Alternativa 1: m?todo rawQuery()
		Cursor contenido = gdb.rawQuery("SELECT * FROM " + TABLE_NAME, null);

		try {
			if (contenido.moveToFirst()) {
				// Recorremos el cursor hasta que no haya m?s registros
				do {
					Grupo g = new Grupo();
					g.setId_grupo(contenido.getInt(0));
					g.setNombre(contenido.getString(1));
					g.setAplicacionesGrupo(AplicacionesXGrupoSQLiteHelper.obtenerListaApp(adb, udb, gdb, axgdb, uxgdb, contenido.getInt(0)));
					g.setUsuariosGrupo(UsuariosXGrupoSQLiteHelper.obtenerListaUsuarios(udb, uxgdb, contenido.getInt(0)));
					g.setPropietario(UsuariosSQLiteHelper.obtenerUsuario(udb,contenido.getInt(4)));
					grupos.add(g);
				} while (contenido.moveToNext());
			}
		} finally {
			contenido.close();
		}


		return grupos;
	}

	public static void eliminarGrupo(SQLiteDatabase gdb) {

		try {
			gdb.execSQL(SQL_TRUNCATE_TABLE);
		} catch (SQLException e) {
			Log.e("SQLEXCEPTION", "" + e);
		}
	}
}
