package uma.inftel.skins4.sqlite;

import java.util.ArrayList;
import java.util.List;

import uma.inftel.skins4.modelo.ComponenteBasico;
import uma.inftel.skins4.modelo.ComponenteXAplicacion;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ComponentesXAppSQLiteHelper extends SQLiteOpenHelper{
	
		public static final String TABLE_NAME = "componentexaplicacion";
		public static final String COLUMN_NAME_ID_CXADB = "id_cxadb";
	    public static final String COLUMN_NAME_ID_CXA = "id_componentexaplicacion";
	    public static final String COLUMN_NAME_ID_APP = " id_aplicacion";
	    public static final String COLUMN_NAME_ID_COMP = " id_componente";
	    public static final String COLUMN_NAME_COL = " columna";
	    public static final String COLUMN_NAME_DETALLES = " detalles";
		
	    private static final String TEXT_TYPE = " TEXT";
	    private static final String INTEGER_TYPE = " INTEGER";
	    private static final String COMMA_SEP = ",";
	    
	    private static final String SQL_CREATE_TABLE =
	        "CREATE TABLE " + TABLE_NAME + " (" +
	        COLUMN_NAME_ID_CXADB + " INTEGER PRIMARY KEY AUTOINCREMENT," +
	        COLUMN_NAME_ID_CXA + " INTEGER," +
	        COLUMN_NAME_ID_APP + INTEGER_TYPE + COMMA_SEP +
	        COLUMN_NAME_ID_COMP + INTEGER_TYPE + COMMA_SEP +
	        COLUMN_NAME_COL + TEXT_TYPE + COMMA_SEP +
	        COLUMN_NAME_DETALLES + INTEGER_TYPE +
	        " )";
	    
	    private static final String SQL_DELETE_TABLE =
	    	    "DROP TABLE IF EXISTS " + TABLE_NAME;
	    
	    private static final String SQL_TRUNCATE_TABLE =
	    	    "DELETE FROM " + TABLE_NAME;
	 
	    public ComponentesXAppSQLiteHelper(Context contexto, String nombre, CursorFactory factory, int version) {
	        super(contexto, nombre, factory, version);
	    }
	 
	    @Override
	    public void onCreate(SQLiteDatabase db) {
	        //Se ejecuta la sentencia SQL de creaci?n de la tabla
	        db.execSQL(SQL_CREATE_TABLE);
	    }
	 
	    @Override
	    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {
	        //NOTA: Por simplicidad del ejemplo aqu? utilizamos directamente la opci?n de
	        //      eliminar la tabla anterior y crearla de nuevo vac?a con el nuevo formato.
	        //      Sin embargo lo normal ser? que haya que migrar datos de la tabla antigua
	        //      a la nueva, por lo que este m?todo deber?a ser m?s elaborado.
	 
	        //Se elimina la versi?n anterior de la tabla
	        db.execSQL(SQL_DELETE_TABLE);
	 
	        //Se crea la nueva versi?n de la tabla
	        db.execSQL(SQL_CREATE_TABLE);
	    }
	    
	    public static void insertarCXA(SQLiteDatabase cXadb, ComponenteXAplicacion cxa){
	    	
	    	ContentValues nuevoRegistro = new ContentValues();
	    	
	    	nuevoRegistro.put("id_componentexaplicacion", cxa.getId_CXA());
	    	nuevoRegistro.put("id_aplicacion", cxa.getAplicacion().getId());
	    	nuevoRegistro.put("id_componente", cxa.getComponente().getId_componente());
	    	nuevoRegistro.put("columna", cxa.getCol());
	    	if(cxa.getDetalles() != null)
	    		nuevoRegistro.put("detalles", cxa.getDetalles());
	    	
	    	cXadb.insert(TABLE_NAME, null, nuevoRegistro);
	    }
	    
	    public static List<ComponenteXAplicacion> obtenerListaCXA(SQLiteDatabase cXadb, SQLiteDatabase adb, SQLiteDatabase udb, SQLiteDatabase gdb, SQLiteDatabase axgdb, SQLiteDatabase uxgdb, Integer id){
	        
	    	List<ComponenteXAplicacion> componentes = new ArrayList<ComponenteXAplicacion>();

	    	//Alternativa 1: m?todo rawQuery()

			String[] args = new String[] { id.toString() };

			Cursor contenido = cXadb.query(TABLE_NAME, null, "id_aplicacion=?", args, null, null,null);

	    	try {
				if (contenido.moveToFirst()) {
				     //Recorremos el cursor hasta que no haya m?s registros
				     do {
				    	  ComponenteXAplicacion comp  = new ComponenteXAplicacion();
				    	  comp.setId_CXA(contenido.getInt(1));
				    	  comp.setAplicacion(AplicacionesSQLiteHelper.obtenerAppSinGrupos(adb, udb, gdb, axgdb, uxgdb, contenido.getInt(2)));
				    	  comp.setComponente(new ComponenteBasico(contenido.getInt(3)));
				    	  comp.setCol(contenido.getString(4));
				    	  comp.setDetalles(contenido.getString(5));
				          componentes.add(comp);
				     } while(contenido.moveToNext());
				}
			} finally {
				contenido.close();
			}
	    	
	    	return componentes;
	    }
	    
	    public void obtenerDatos(SQLiteDatabase db){
		    
	    	ComponenteXAplicacion cxa = new ComponenteXAplicacion();
	    	Cursor contenido = db.rawQuery("SELECT * FROM "+TABLE_NAME, null);

	    	try {
				if(contenido != null) {
					contenido.moveToFirst();
					
					cxa.setId_CXA(contenido.getInt(0));
//	    		cxa.setId_CXA(contenido.getInt(1));
//	    		cxa.setIdComp(contenido.getInt(2));
					cxa.setCol(contenido.getString(3));
					cxa.setDetalles(contenido.getString(4));
				}
			} finally {
				contenido.close();
			}

	    }
	    
	    public static void eliminarCXA(SQLiteDatabase cXadb){
	    	try{
	    		cXadb.execSQL(SQL_TRUNCATE_TABLE);
	    	}
	    	catch (SQLException e)
	    	{
	    		Log.e("SQLEXCEPTION", ""+e);
	    	}
	    }
}
