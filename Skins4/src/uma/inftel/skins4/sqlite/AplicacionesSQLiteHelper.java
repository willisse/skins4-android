package uma.inftel.skins4.sqlite;

import java.util.ArrayList;
import java.util.List;

import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.ComponenteXAplicacion;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AplicacionesSQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME = "aplicacion";
	public static final String COLUMN_NAME_ID_APP = "id_aplicacion";
	public static final String COLUMN_NAME_NOMBRE = " nombre";
	public static final String COLUMN_NAME_DESC = " descripcion";
	public static final String COLUMN_NAME_FECHA = " fecha";
	public static final String COLUMN_NAME_PROPIETARIO = " propietario";
	public static final String COLUMN_NAME_GRUPO = " grupo";

	private static final String TEXT_TYPE = " TEXT";
	private static final String INTEGER_TYPE = " INTEGER";
	private static final String COMMA_SEP = ",";
	private static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
			+ " (" + COLUMN_NAME_ID_APP + " INTEGER PRIMARY KEY,"
			+ COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP + COLUMN_NAME_DESC
			+ TEXT_TYPE + COMMA_SEP + COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP
			+ COLUMN_NAME_PROPIETARIO + INTEGER_TYPE + COMMA_SEP
			+ COLUMN_NAME_GRUPO + INTEGER_TYPE + " )";

	private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;

	private static final String SQL_TRUNCATE_TABLE = "DELETE FROM "
			+ TABLE_NAME;

	public AplicacionesSQLiteHelper(Context contexto, String nombre,
			CursorFactory factory, int version) {
		super(contexto, nombre, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Se ejecuta la sentencia SQL de creaci?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior,
			int versionNueva) {
		// NOTA: Por simplicidad del ejemplo aqu? utilizamos directamente la
		// opci?n de
		// eliminar la tabla anterior y crearla de nuevo vac?a con el nuevo
		// formato.
		// Sin embargo lo normal ser? que haya que migrar datos de la tabla
		// antigua
		// a la nueva, por lo que este m?todo deber?a ser m?s elaborado.

		// Se elimina la versi?n anterior de la tabla
		db.execSQL(SQL_DELETE_TABLE);

		// Se crea la nueva versi?n de la tabla
		db.execSQL(SQL_CREATE_TABLE);
	}

	public static void insertarApp(SQLiteDatabase adb, SQLiteDatabase cXadb,
			Aplicacion app) {

		if (app != null && !existeApp(adb, app.getId())) {
			ContentValues nuevoRegistro = new ContentValues();
			nuevoRegistro.put("id_aplicacion", app.getId());
			nuevoRegistro.put("nombre", app.getNombre());
			nuevoRegistro.put("descripcion", app.getDescripcion());
			nuevoRegistro.put("fecha", app.getFecha());
			nuevoRegistro.put("propietario", app.getPropietario()
					.getId_usuario());
			if (app.getGrupo() != null)
				nuevoRegistro.put("grupo", app.getGrupo().getId_grupo());

			adb.insert(TABLE_NAME, null, nuevoRegistro);

			if (app.getListaCompnentes() != null && !app.getListaCompnentes().isEmpty()) {
				for (ComponenteXAplicacion cxa : app.getListaCompnentes()) {
					ComponentesXAppSQLiteHelper.insertarCXA(cXadb, cxa);
				}
			}
		}
	}

	public static Boolean existeApp(SQLiteDatabase adb, Integer id){
	
		String[] args = new String[] { id.toString() };
		Boolean existe = false;

		Cursor contenido = adb.query(TABLE_NAME, null, "id_aplicacion=?", args, null, null, null);
		
		try {
			if (contenido.moveToFirst()) {
				existe = true;
			}
		} finally {
			contenido.close();
		}
		
		return existe;
	}
	
	public static Aplicacion obtenerApp(SQLiteDatabase adb, SQLiteDatabase udb,
			SQLiteDatabase gdb, SQLiteDatabase axgdb, SQLiteDatabase uxgdb,
			Integer id) {

		Aplicacion aplicacion = null;
		String[] args = new String[] { id.toString() };

		Cursor contenido = adb.query(TABLE_NAME, null, "id_aplicacion=?", args,
				null, null, null);

		try {
			if (contenido.moveToFirst()) {
				aplicacion = new Aplicacion();
				aplicacion.setId(contenido.getInt(0));
				aplicacion.setNombre(contenido.getString(1));
				aplicacion.setDescripcion(contenido.getString(2));
				aplicacion.setFecha(contenido.getString(3));
				aplicacion.setPropietario(UsuariosSQLiteHelper.obtenerUsuario(
						udb, contenido.getInt(4)));
				aplicacion.setGrupo(GruposSQLiteHelper.obtenerGrupo(adb, udb,
						gdb, axgdb, uxgdb, contenido.getInt(5)));
			}
		} finally {
			contenido.close();
		}

		return aplicacion;
	}

	public static Aplicacion obtenerAppSinGrupos(SQLiteDatabase adb,
			SQLiteDatabase udb, SQLiteDatabase gdb, SQLiteDatabase axgdb,
			SQLiteDatabase uxgdb, Integer id) {

		Aplicacion aplicacion = new Aplicacion();
		String[] args = new String[] { id.toString() };

		Cursor contenido = adb.query(TABLE_NAME, null, "id_aplicacion=?", args,
				null, null, null);

		try {
			if (contenido.moveToFirst()) {
				aplicacion.setId(contenido.getInt(0));
				aplicacion.setNombre(contenido.getString(1));
				aplicacion.setDescripcion(contenido.getString(2));
				aplicacion.setFecha(contenido.getString(3));
				aplicacion.setPropietario(UsuariosSQLiteHelper.obtenerUsuario(
						udb, contenido.getInt(4)));
				// aplicacion.setGrupo(GruposSQLiteHelper.obtenerGrupo(adb, udb,
				// gdb, axgdb, uxgdb, contenido.getInt(5)));
			}
		} finally {
			contenido.close();
		}

		return aplicacion;
	}

	public static List<Aplicacion> obtenerListaApp(SQLiteDatabase adb,
			SQLiteDatabase udb, SQLiteDatabase gdb, SQLiteDatabase axgdb, SQLiteDatabase cXadb,
			SQLiteDatabase uxgdb) {

		List<Aplicacion> aplicaciones = new ArrayList<Aplicacion>();

		// Alternativa 1: m?todo rawQuery()
		Cursor contenido = adb.rawQuery("SELECT * FROM " + TABLE_NAME, null);

		try {
			if (contenido.moveToFirst()) {
				// Recorremos el cursor hasta que no haya m?s registros
				do {
					Aplicacion app = new Aplicacion();
					app.setId(contenido.getInt(0));
					app.setNombre(contenido.getString(1));
					app.setDescripcion(contenido.getString(2));
					app.setFecha(contenido.getString(3));
					app.setPropietario(UsuariosSQLiteHelper.obtenerUsuario(udb,
							contenido.getInt(4)));
					app.setGrupo(GruposSQLiteHelper.obtenerGrupoSinApp(adb,
							udb, gdb, axgdb, uxgdb, contenido.getInt(5)));
					
					app.setListaCompnentes(ComponentesXAppSQLiteHelper.obtenerListaCXA(cXadb, adb, udb, gdb, axgdb, uxgdb, app.getId()));
					aplicaciones.add(app);
				} while (contenido.moveToNext());
			}
		} finally {
			contenido.close();
		}

		return aplicaciones;
	}

	public static void eliminarApp(SQLiteDatabase adb) {
		try {
			adb.execSQL(SQL_TRUNCATE_TABLE);
		} catch (SQLException e) {
			Log.e("SQLEXCEPTION", "" + e);
		}
	}
}
