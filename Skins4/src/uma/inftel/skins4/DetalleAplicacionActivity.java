package uma.inftel.skins4;

import uma.inftel.skins4.adapters.GridViewAdapter;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

public class DetalleAplicacionActivity extends Activity {

	private Aplicacion aplicacion;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostrador);
		getActionBar().setDisplayHomeAsUpEnabled(false);		
		aplicacion = (Aplicacion) getIntent().getSerializableExtra("aplicacion");
		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(new GridViewAdapter(DetalleAplicacionActivity.this, aplicacion));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.detalleaplicacion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Log.i("ActionBar", "Settings!");;
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onDestroy () {
		super.onDestroy();
	}

}
