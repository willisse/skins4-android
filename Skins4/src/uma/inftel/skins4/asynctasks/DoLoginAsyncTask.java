package uma.inftel.skins4.asynctasks;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import uma.inftel.skins4.R;
import uma.inftel.skins4.conexiones.Conexion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;


public class DoLoginAsyncTask extends AsyncTask<String, Void, JSONObject> {

	private ProgressDialog dialog;
	private String url;
	private String method;
	Context c;

	public DoLoginAsyncTask(Context c) {
		this.c = c;
		dialog = new ProgressDialog(c);
		url = c.getString(R.string.ip) + ":" + c.getString(R.string.puerto)
				+ c.getString(R.string.doLoginPath);
		method = "POST";

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Autenticando usuario...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONObject doInBackground(String... arg0) {
		try {
			Conexion conn = new Conexion();
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("email", arg0[0]);
			params.put("password", arg0[1]);
			return new JSONObject(conn.doConnection(url, params, method));
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(JSONObject result) {
		super.onPostExecute(result);
		// Hacer todo el post-procesamiento. Guardado en usuario singleton,
		// navegacion a la siguiente activity, etc...
		try {
			UsuarioSingleton.initUsuario(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		SharedPreferences sh = c.getSharedPreferences("credenciales",Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sh.edit();
		editor.putString("user", result.toString());
		editor.commit();
		
		if (dialog.isShowing()){
			dialog.dismiss();
		}
		CargarTodoAsyncTask ct = new CargarTodoAsyncTask(this.c);
		ct.execute();
		
	}

	
}
