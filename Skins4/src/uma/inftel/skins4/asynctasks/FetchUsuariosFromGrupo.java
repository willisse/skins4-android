package uma.inftel.skins4.asynctasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import uma.inftel.skins4.DetalleGrupoFragment;
import uma.inftel.skins4.R;
import uma.inftel.skins4.conexiones.Conexion;
import uma.inftel.skins4.modelo.Grupo;
import uma.inftel.skins4.modelo.Usuario;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class FetchUsuariosFromGrupo extends AsyncTask<String, Void, JSONArray>
		implements CallBackInterface {

	private ProgressDialog dialog;
	private String url;
	private String method;
	private Grupo grupo;
	Context c;

	public FetchUsuariosFromGrupo(Context c, Grupo grupo) {
		this.c = c;
		dialog = new ProgressDialog(c);
		method = "GET";
		url = c.getString(R.string.ip) + ":" + c.getString(R.string.puerto)
				+ c.getString(R.string.fetchUsuariosFromGrupoRelPath);
		this.grupo = grupo;

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Cargando Usuarios... ");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONArray doInBackground(String... params) {
		try {
			Conexion conn = new Conexion();
			HashMap<String, String> parametros = new HashMap<String, String>();
			parametros.put("idGrupo", grupo.getId_grupo().toString());
			JSONArray js = new JSONArray(conn.doConnection(url, parametros,
					method));

			return js;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void onPostExecute(JSONArray result) {
		super.onPostExecute(result);
		List<Usuario> usuariosGrupo = new ArrayList<Usuario>();
		for (int i = 0; i < result.length(); i++) {
			try {
				usuariosGrupo.add(new Usuario(result.getJSONObject(i)));
			} catch (JSONException e) {

				e.printStackTrace();
			}

		}

		grupo.setUsuariosGrupo(usuariosGrupo);
		new FetchAplicacionesGivenGroupAsyncTask(c, grupo, this).execute();

	}

	@Override
	public void onLoadCompleted(int codigo, String string) {

		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		if (codigo == 0) {
			Intent intent = new Intent(this.c, DetalleGrupoFragment.class);
			intent.putExtra("grupo", grupo);
			c.startActivity(intent);
		}

	}

}
