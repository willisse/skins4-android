package uma.inftel.skins4.asynctasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import uma.inftel.skins4.DetalleAplicacionActivity;
import uma.inftel.skins4.R;
import uma.inftel.skins4.conexiones.Conexion;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.ComponenteXAplicacion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;


public class FetchComponentesAplicacionAsyncTask extends AsyncTask<Integer, Void, JSONArray>{

	private ProgressDialog dialog;
	private String url;
	private String method;
	private Context c;
	private Aplicacion aplicacion;


	public FetchComponentesAplicacionAsyncTask (Context c, Aplicacion app){
		this.c = c;
		this.aplicacion = app;
		dialog = new ProgressDialog(c);
		url = c.getString(R.string.ip) + ":" + c.getString(R.string.puerto) + c.getString(R.string.fetchComponentesAplicacionRelPath);
		method = "GET";
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Leyendo componentes de la aplicacion...");
		dialog.setIndeterminate(true);
		dialog.show();
	}


	@Override
	protected JSONArray doInBackground(Integer... params) {
		try{
			HashMap<String, String> parametros = new HashMap<String, String>();
			parametros.put("idAplicacion", aplicacion.getId().toString());
			Conexion c = new Conexion();
			return new JSONArray (c.doConnection(url, parametros, method));
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(JSONArray result){
		super.onPostExecute(result);
		
		List<ComponenteXAplicacion> componentesApp = new ArrayList<ComponenteXAplicacion>();
		for (int i = 0; i < result.length(); i++) {
			try {
				componentesApp.add(new ComponenteXAplicacion(result.getJSONObject(i)));
			} catch (JSONException e) {

				e.printStackTrace();
			}

		}

		aplicacion.setListaCompnentes(componentesApp);

		
		//Hacer todo el post-procesamiento
		/*try {
			UsuarioSingleton.initCXA(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		if (dialog.isShowing()){
			dialog.dismiss();
		}
		
		Intent i = new Intent(c, DetalleAplicacionActivity.class);
		i.putExtra("aplicacion", aplicacion);
		c.startActivity(i);

	}
}
