package uma.inftel.skins4.asynctasks;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import uma.inftel.skins4.R;
import uma.inftel.skins4.conexiones.Conexion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.content.Context;
import android.os.AsyncTask;


public class FetchAplicacionesAsynctask extends
		AsyncTask<String, Void, JSONArray> {

	private String url;
	private String method;
	private CallBackInterface call;
	private final int STATUS_OK = 1;
	private final int STATUS_FAIL = -1;
	
	public FetchAplicacionesAsynctask(Context c, CallBackInterface call){
		method = "GET";
		url = c.getString(R.string.ip) + ":" + c.getString(R.string.puerto) + c.getString(R.string.fetchAplicacionesPath);
		this.call = call;
	}
	
	
	@Override
	protected JSONArray doInBackground(String... arg0) {
		try{
			Thread.sleep(5000);
			Conexion conn = new Conexion();
			HashMap <String, String> params = new HashMap<String, String>();
			params.put("email", arg0[0]);
			return new JSONArray (conn.doConnection(url, params, method));
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		catch (IOException e){
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute (JSONArray result){
		super.onPostExecute(result);
		try {
			UsuarioSingleton.initApps(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result != null)
			call.onLoadCompleted(STATUS_OK, FetchAplicacionesAsynctask.class.toString());
		else
			call.onLoadCompleted(STATUS_FAIL, FetchAplicacionesAsynctask.class.toString());
	}
}
