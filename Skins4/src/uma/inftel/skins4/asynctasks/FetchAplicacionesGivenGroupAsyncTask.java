package uma.inftel.skins4.asynctasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import uma.inftel.skins4.R;
import uma.inftel.skins4.conexiones.Conexion;
import uma.inftel.skins4.modelo.Aplicacion;
import uma.inftel.skins4.modelo.Grupo;
import android.content.Context;
import android.os.AsyncTask;

public class FetchAplicacionesGivenGroupAsyncTask extends
AsyncTask<Integer, Void, JSONArray> {

	
	private String url;
	private String method;
	private CallBackInterface call;
	private Grupo grupo;
	private final int STATUS_OK = 0;
	private final int STATUS_FAIL = -1;
	private Context c;
	

	public FetchAplicacionesGivenGroupAsyncTask(Context c, Grupo grupo, CallBackInterface call){
		this.c = c;
		url = c.getString(R.string.ip) + ":" + c.getString(R.string.puerto) + c.getString(R.string.fetchAplicacionesGivenGroupPath);
		method = "GET";
		this.call = call;
		this.grupo = grupo;
	}

	
	@Override
	protected JSONArray doInBackground(Integer... params) {
		try{
			HashMap<String, String> parametros = new HashMap<String, String>();
			parametros.put("idGrupo", grupo.getId_grupo().toString());
			Conexion c = new Conexion();
			return new JSONArray (c.doConnection(url, parametros, method));

		}
		catch (JSONException e){
			e.printStackTrace();
		}
		catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}

	protected void onPostExecute(JSONArray result){
		super.onPostExecute(result);

		List<Aplicacion> listaAplicaciones = new ArrayList<Aplicacion>();
		//Hacer todo el post-procesamiento. Mostrar las aplicaciones en su activity correspondiente y tal...
		for (int i = 0; i < result.length(); i++) {
			try {
				listaAplicaciones.add(new Aplicacion(result.getJSONObject(i)));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}
		
		grupo.setAplicacionesGrupo(listaAplicaciones);
		call.onLoadCompleted(STATUS_OK, FetchAplicacionesGivenGroupAsyncTask.class.toString());


	}
}