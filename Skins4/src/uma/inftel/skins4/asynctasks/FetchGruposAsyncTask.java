package uma.inftel.skins4.asynctasks;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import uma.inftel.skins4.R;
import uma.inftel.skins4.conexiones.Conexion;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class FetchGruposAsyncTask extends AsyncTask<Integer, Void, JSONArray> {

	private String url;
	private String method;
	private CallBackInterface call;
	private final int OK_CODE = 1;
	private final int FAIL_CODE = -1;

	public FetchGruposAsyncTask (Context c, CallBackInterface call){
		url = c.getString(R.string.ip) + ":" + c.getString(R.string.puerto) + c.getString(R.string.fetchGruposRelPath);
		method = "GET";
		this.call = call;
	}


	@Override
	protected JSONArray doInBackground(Integer... params) {
		try{
			HashMap<String, String> parametros = new HashMap<String, String>();
			parametros.put("idUsuario", params[0].toString());
			Conexion c = new Conexion();
			return new JSONArray (c.doConnection(url, parametros, method));
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}

	protected void onPostExecute(JSONArray result){
		super.onPostExecute(result);
		//Postprocesamiento
		if (result != null){
			try {
				UsuarioSingleton.initGrupos(result);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			this.call.onLoadCompleted(OK_CODE, FetchGruposAsyncTask.class.toString());
		}
		else
			this.call.onLoadCompleted(FAIL_CODE, FetchGruposAsyncTask.class.toString());
	}
}
