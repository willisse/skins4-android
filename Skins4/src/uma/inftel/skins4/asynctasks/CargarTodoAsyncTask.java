package uma.inftel.skins4.asynctasks;

import java.util.HashMap;
import java.util.Map.Entry;

import uma.inftel.skins4.PrincipalFragment;
import uma.inftel.skins4.singleton.UsuarioSingleton;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class CargarTodoAsyncTask extends AsyncTask<Void, Integer, Void> implements CallBackInterface {

	private ProgressDialog dialog;
	private Context c;
	private Integer progress;
	private HashMap<String, Integer> tareasLanzadas;
	
	private final int NUM_THREADS = 2;
	private final int OK_CODE = 1;
	private final int TRABAJANDO_CODE = 0;
	private final int FAIL_CODE = -1;
	private final int DIALOG_MAX_PROGRESS = 100;

	public CargarTodoAsyncTask (Context c){
		this.c = c;
		dialog = new ProgressDialog(c);
		dialog.setIndeterminate(false);
		dialog.setMax(DIALOG_MAX_PROGRESS);
		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progress = 0;
		//Key: nombre del asyncTask lanzado
		//Value: Respuesta del AsyncTask -> 
			//0: no respuesta aun
			//1: codigo OK
			//-1: codigo FAIL
		tareasLanzadas = new HashMap<String, Integer>();
		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Cargando datos de la aplicacion...");
		dialog.setProgress(0);
		dialog.show();
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		//Lanzamos todos los AsyncTask que cargaran mucha de la informacion del usuario.
		FetchAplicacionesAsynctask fa = new FetchAplicacionesAsynctask(c, this);
		fa.execute(UsuarioSingleton.getInstance().getUsuario().getLogin());
		tareasLanzadas.put(FetchAplicacionesAsynctask.class.toString(), 0);
		FetchGruposAsyncTask fg = new FetchGruposAsyncTask(c, this);
		fg.execute(UsuarioSingleton.getInstance().getUsuario().getId_usuario());
		tareasLanzadas.put(FetchGruposAsyncTask.class.toString(), 0);
		return null;
	}

	protected void onProgressUpdate(Integer... progress) {
		dialog.setProgress((this.progress*100)/NUM_THREADS);
	}



	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}


	public synchronized void onLoadCompleted(int codigo, String nombre){
		boolean terminado = true;
		tareasLanzadas.put(nombre, codigo);
		for (Entry<String, Integer> val : tareasLanzadas.entrySet()){
			if (val.getValue() == TRABAJANDO_CODE){
				terminado = false;
				Log.e("NO TERMINADO", "Aun falta por terminar: " + val.getKey());
			}
			else if (val.getValue() == FAIL_CODE){
				Log.e("ERROR", "Error cargando " + val.getKey());
			}
			else if (val.getValue() == OK_CODE){
				progress++;
				publishProgress(progress);
			}
		}
		if (terminado){
			Log.e("TERMINADO", "Han terminado todas las tareas, lanzamos el fragment principal");
			if (dialog.isShowing()){
				dialog.dismiss();
			}
			Intent i = new Intent(c, PrincipalFragment.class);
			c.startActivity(i);
		}
	}


}
